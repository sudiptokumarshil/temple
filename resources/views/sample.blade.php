<div class="content-wrapper">
    <style>
        .myTable {
            width: 100%;
            text-align: left;
            background-color: lemonchiffon;
            border-collapse: collapse;
        }

        .myTable th {
            background-color: red;
            color: white;
        }

        .myTable td,
        .myTable th {
            padding: 10px;
            border: 1px solid green;
        }

        .blink {
            animation: blinker 4s linear infinite;
            color: yellow;
            font-size: 20px;
            font-weight: bold;
            font-family: sans-serif;
        }

        .custom-card {
            background-color: #318777;
            color: white;
            /* #17A2B8; */
        }

        .card-body {
            background-color: lemonchiffon;
        }

        @keyframes blinker {
            50% {
                opacity: 0;
            }
        }

    </style>
    <div class="row">
        <div class="col-xs-12">
            <h4 class="page-title"> </h4>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header custom-card text-center"><b class="blink"></b></div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            {{-- input from here --}}

                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <div class="form-group text-center">
                                    <label for="exampleFormControlSelect1"></label>
                                    {{-- button from here --}}
                                </div>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    </div>
                    <div class='text-right'>

                    </div>
                    <p id="dateMessage" style="color: red" class="text-center"></p>
                    <div id="reportResult">
                        <table id="example"
                            class="display myTable datatable table-responsive w-100 d-block d-md-table table table-stripped table-bordered mt-3">

                        </table>
                    </div>
                    <div class="d-flex justify-content-end">

                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
</div>
