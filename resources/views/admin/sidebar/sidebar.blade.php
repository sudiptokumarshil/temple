 <nav class="mt-2">
     <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         <li class="nav-item has-treeview menu-open">
             <a href="{{ route('home') }}" class="nav-link active">
                 <i class="nav-icon fas fa-tachometer-alt"></i>
                 <p>
                     Dashboard
                     <!--<i class="right fas fa-angle-left"></i>-->
                 </p>
             </a>

@if (Auth::user()->role_id == 1 || Auth::user()->role_id == 6 || Auth::user()->role_id == 3 || Auth::user()->role_id == 9)
         <li class="nav-item has-treeview">
             <a href="#" class="nav-link">
                 <i class="nav-icon fas fa-copy"></i>
                 <p>
                     Book Stall
                     <i class="fas fa-angle-left right"></i>
                     <span class="badge badge-info right"></span>
                 </p>
             </a>

             <ul class="nav nav-treeview">
                 <li class="nav-item">
                     @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 6 || Auth::user()->role_id == 3)
                         <a href='{{ route('add-book') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>Add Item </p>
                         </a>


                         <a href='{{ route('bookstall-report') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>Sale Report</p>
                         </a>
                         <a href='{{ route('itemstock-report') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>Item Stock Report</p>
                         </a>

                         <a href='{{ route('bookstall-wasted') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>Wasted Item</p>
                         </a>
                     @endif
                     @if (Auth::user()->role_id == 1 || Auth::user()->role_id == 6 || Auth::user()->role_id == 3 || Auth::user()->role_id == 9)
                         <a href='{{ route('all-book') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>All Item</p>
                         </a>
                         <a href='{{ route('bookstall-sale') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>Sale</p>
                         </a>
                     @endif
                 </li>
             </ul>

         </li>
@endif

@if (Auth::user()->role_id == 2 || Auth::user()->role_id == 6 || Auth::user()->role_id == 3 || Auth::user()->role_id == 8)
         <!--Bakery Part-->
         <li class="nav-item has-treeview">
             <a href="#" class="nav-link">
                 <i class="nav-icon fas fa-copy"></i>
                 <p>
                     Bakery
                     <i class="fas fa-angle-left right"></i>
                     <span class="badge badge-info right"></span>
                 </p>
             </a>

             <ul class="nav nav-treeview">
                 <li class="nav-item">
                     @if (Auth::user()->role_id == 2 || Auth::user()->role_id == 6 || Auth::user()->role_id == 3)
                         <a href='{{ route('add-item') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>Add Item </p>
                         </a>
                         <a href='{{ route('bakery-wasted') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>wasted Item</p>
                         </a>
                         <a href='{{ route('bakery-stock-report') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>Item Stock Report</p>
                         </a>
                         <a href='{{ route('bakery-report') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>Sales Report</p>
                         </a>
                     @endif
                     @if (Auth::user()->role_id == 2 || Auth::user()->role_id == 6 || Auth::user()->role_id == 3 || Auth::user()->role_id == 8)
                         <a href='{{ route('all-item') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>All Item</p>
                         </a>
                         <a href='{{ route('bakery-sales') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>Sale</p>
                         </a>
                     @endif
                 </li>
             </ul>

         </li>
@endif

         <!--Office-->
         @if (Auth::user()->role_id == 5 || Auth::user()->role_id == 7 || Auth::user()->role_id == 3)
         <li class="nav-item has-treeview">
             <a href="#" class="nav-link">
                 <i class="nav-icon fas fa-copy"></i>
                 <p>
                     Office
                     <i class="fas fa-angle-left right"></i>
                     <span class="badge badge-info right"></span>
                 </p>
             </a>
             @if (Auth::user()->role_id == 5 || Auth::user()->role_id == 7 || Auth::user()->role_id == 3)
                 <ul class="nav nav-treeview">
                     <li class="nav-item">
                         <a href="{{ route('manage-contact') }}" class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>All Contact</p>
                         </a>
                         <a href="{{ route('manage-office-member') }}" class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>All Member</p>
                         </a>
                     </li>
                 </ul>
             @endif
         </li>
         @endif

         <!--Netya Seva-->
         @if (Auth::user()->role_id == 4 || Auth::user()->role_id == 7 || Auth::user()->role_id == 3)
         <li class="nav-item has-treeview">
             <a href="#" class="nav-link">
                 <i class="nav-icon fas fa-copy"></i>
                 <p>
                     Nitya Seva
                     <i class="fas fa-angle-left right"></i>
                     <span class="badge badge-info right"></span>
                 </p>
             </a>
             @if (Auth::user()->role_id == 4 || Auth::user()->role_id == 7 || Auth::user()->role_id == 3)
                 <ul class="nav nav-treeview">
                     <li class="nav-item">
                         <a href='{{ route('add-customer') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>Add Member</p>
                         </a>
                     </li>
                     <li class="nav-item">
                         <a href='{{ route('all-customer') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>All Member</p>
                         </a>
                     </li>
                     <li class="nav-item">
                         <a href='{{ route('add-pranami') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>Pranami Form</p>
                         </a>
                     </li>
                     <li class="nav-item">
                         <a href='{{ route('pranami-report') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>Pranami Report</p>
                         </a>
                     </li>
                 </ul>
             @endif
         </li>
         @endif
         @if (Auth::user()->role_id == 3 || Auth::user()->role_id == 3)
             <!--Roles -->
             <li class="nav-item has-treeview">
                 <a href="#" class="nav-link">
                     <i class="nav-icon fas fa-copy"></i>
                     <p>
                         Role Management
                         <i class="fas fa-angle-left right"></i>
                         <span class="badge badge-info right"></span>
                     </p>
                 </a>

                 <ul class="nav nav-treeview">
                     <li class="nav-item">
                         <a href='{{ route('manage-role') }}' class="nav-link">
                             <i class="far fa-circle nav-icon"></i>
                             <p>Roles</p>
                         </a>
                     </li>
                 </ul>

             </li>
         @endif

         <li class="nav-item">
             <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                 {{ __('Logout') }}
             </a>

             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
             </form>
         </li>

 </nav>
