@extends('admin.master')
@section('home')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Update Book</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Book Code</label>
                                        <input type="text" required name='item_code' class="form-control"
                                            value='{{ $book->item_code }}' id="item_code" placeholder="Enter Item Code">
                                        <input type="hidden" name='id' class="form-control" value='{{ $book->id }}'
                                            id="item_id" placeholder="Enter Item Code">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Book Name</label>
                                        <input type="text" required name='item_name' class="form-control"
                                            value='{{ $book->item_name }}' id="item_name" placeholder="Enter Item Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"> Quantity</label>
                                        <input type="number" required name='quantity' class="form-control"
                                            value='{{ $book->quantity }}' id="quantity" placeholder="Enter Item Quantity">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"> Price</label>
                                        <input type="number" required name='price' class="form-control"
                                            value='{{ $book->price }}' id="price" placeholder="Enter Item Price">
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button onclick='updateBook(this)' type="button" class="btn btn-primary btn-sm">Update</button>
                                    <a href='{{ route('all-book') }}' type='button' class='btn btn-success btn-sm'
                                        style='margin:10px'> All Book </a>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <script src='{{ asset('/') }}public/custom/api.js'></script>
    <script src='{{ asset('/') }}public/custom/bookstall.js'></script>
@endsection
