@extends('admin.master')
@section('home')
<style>
    .myTable {
        width: 100%;
        text-align: left;
        background-color: lemonchiffon;
        border-collapse: collapse;
    }

    .myTable th {
        background-color: red;
        color: white;
    }

    .myTable td,
    .myTable th {
        padding: 10px;
        border: 1px solid green;
    }

    .blink {
        animation: blinker 4s linear infinite;
        color: yellow;
        font-size: 20px;
        font-weight: bold;
        font-family: sans-serif;
    }

    .custom-card {
        background-color: #318777;
        color: white;
        /* #17A2B8; */
    }

    .card-body {
        background-color: lemonchiffon;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

</style>
    <div class="content-wrapper">
        <div class='container'>
            <div class='row'>
                <div class='col-md-6'>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1"></label>
                        <input type='text' class='form-control' onkeyup='searchItem(this)'
                            placeholder='Enter Item Code Or Name' name='item_info' id='item_info'>
                    </div>
                </div>

                <div class='col-md-6'>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1"></label>
                        <!--<button type='button' style='margin-top:5%' onclick='searchItem(this)' align='center'  class='btn btn-success'>Search</button>-->
                    </div>
                </div>

            </div>
        </div>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header custom-card">
                                <h3 class="card-title blink">Bookstall All Item</h3>

                            </div>
                            <!-- /.card-header -->
                            <h4 style='color:green' align='center'>{{ Session::get('message') }}</h4>

                            <div class="card-body table-responsive myTable table-sm table-bordered" id='itemData'>
                                <a href='{{ route('add-book') }}' type='button' class='btn btn-success btn-sm' style='margin:10px'>
                                    Add New Item </a>
                                    <table id="example" class="table table-bordered table-hover display nowrap">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>Item Name</th>
                                            <th>Item Code</th>
                                            <th>Received Quantity</th>
                                            <th>Stock</th>
                                            <th>Price</th>
                                            <th>Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($i = 1)
                                            @foreach ($book as $item)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>
                                                        {{ $item->item_name }}
                                                    </td>
                                                    <td>
                                                        {{ $item->item_code }}
                                                    </td>
                                                    <td>
                                                        {{ $item->quantity }}
                                                    </td>
                                                    <td>
                                                        {{ $item->stock }}
                                                    </td>
                                                    <td>
                                                        {{ $item->price }}
                                                    </td>
                                                    <td>
                                                        <a href='{{ route('edit-book', ['id' => $item->id]) }}' type='button'
                                                            class='btn btn-success btn-sm'>Edit</a>
                                                        {{-- <a
                                                            href='{{ route('del-item', ['id' => $item->id]) }}' type='button'
                                                            class='btn btn-danger btn-sm'>Delete</a>
                                                        -->--}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {!! $book->links() !!}
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <script src='{{ asset('/') }}public/custom/api.js'></script>
        <script src='{{ asset('/') }}public/custom/bookstall.js'></script>
    @endsection
