@extends('admin.master')
@section('home')
<style>
    .myTable {
        width: 100%;
        text-align: left;
        background-color: lemonchiffon;
        border-collapse: collapse;
    }

    .myTable th {
        background-color: red;
        color: white;
    }

    .myTable td,
    .myTable th {
        padding: 10px;
        border: 1px solid green;
    }

    .blink {
        animation: blinker 4s linear infinite;
        color: yellow;
        font-size: 20px;
        font-weight: bold;
        font-family: sans-serif;
    }

    .custom-card {
        background-color: #318777;
        color: white;
        /* #17A2B8; */
    }

    .card-body {
        background-color: lemonchiffon;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

</style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">

                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header custom-card">
                        <h1 class="blink">Bookstall</h1>
                    </div>
                    <div class="card-body">
                        <div class='row'>
                            <div class='col-md-4'>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Item Name Or Code</label>
                                    <input type='text' class='form-control item_info' name='item_info' id='item_info'>
                                    <div id='suggest'>

                                    </div>
                                </div>
                            </div>

                            <div class='col-md-4'>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">From </label>
                                    <input type='date' class='form-control' name='from' id='from'>
                                </div>
                            </div>
                            <div class='col-md-4'>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">To </label>
                                    <input type='date' class='form-control' name='to' id='to'>
                                </div>
                            </div>
                            <div class='col-md-4'>
                                <div class="form-group">
                                </div>
                            </div>
                            <div class='col-md-4'>
                                <div class="form-group text-center">
                                    <button type='button'  onclick='itemStockreport(this)'
                                        class='btn btn-success btn-sm'>Search</button>
                                </div>
                            </div>
                            <div class='col-md-4'>
                                <div class="form-group">
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header custom-card">
                                <h1 class="blink">Item stock Report</h1>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body" id='itemstock_result'>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <script src='{{ asset('/') }}public/custom/api.js'></script>
    <script src='{{ asset('/') }}public/custom/bookstall.js'></script>
@endsection
