@extends('admin.master')
@section('home')
<style>
    .myTable {
        width: 100%;
        text-align: left;
        background-color: lemonchiffon;
        border-collapse: collapse;
    }

    .myTable th {
        background-color: red;
        color: white;
    }

    .myTable td,
    .myTable th {
        padding: 10px;
        border: 1px solid green;
    }

    .blink {
        animation: blinker 4s linear infinite;
        color: yellow;
        font-size: 20px;
        font-weight: bold;
        font-family: sans-serif;
    }

    .custom-card {
        background-color: #318777;
        color: white;
        /* #17A2B8; */
    }

    .card-body {
        background-color: lemonchiffon;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

</style>
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header custom-card">
                                <h3 class="card-title blink">Bookstall All Wasted Item</h3>
                            </div>
                            <!-- /.card-header -->
                            <h4 style='color:green' align='center'>{{ Session::get('message') }}</h4>

                            <div class="card-body table-responsive table-sm table-bordered" id='itemData'>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-danger btn-sm mb-3" data-toggle="modal" data-target="#addWasted">
                                    Add Wasted Item
                                </button>
                                <table id="example" class="table myTable table-bordered table-hover mt-3">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>Item Name</th>
                                            <th>Item Code</th>
                                            <th>Quantity</th>
                                            <th>Remarks</th>
                                            <th>Created By</th>
                                            <th>Created At </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($i = 1)
                                            @foreach ($wastedItems as $wastedItem)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>
                                                        {{ $wastedItem->item_name }}
                                                    </td>
                                                    <td>
                                                        {{ $wastedItem->item_code }}
                                                    </td>
                                                    <td>
                                                        {{ $wastedItem->quantity }}
                                                    </td>
                                                    <td>
                                                        {{ $wastedItem->remarks }}
                                                    </td>
                                                    @if ($wastedItem->user_name)
                                                        <td>
                                                            <p style="color:green">{{ $wastedItem->user_name }}</p>
                                                        </td>
                                                    @else
                                                        <td>
                                                            <p style="color:red">Not Found</p>
                                                        </td>
                                                    @endif
                                                    <td>
                                                        {{ $wastedItem->created_at }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>

                                    </table>
                                    {{ $wastedItems->links()}}
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <script src='{{ asset('/') }}public/custom/api.js'></script>
        <script src='{{ asset('/') }}public/custom/bookstall.js'></script>
        @include('admin.bookstall.modal.add_wasted')
        <script>
            $(document).ready(function() {
                $('.searchwItem').select2();
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [{
                            extend: 'copy',
                            exportOptions: {
                                columns: [0, 1, 2, 3,4,5,6]
                            }
                        },
                        {
                            extend: 'excel',
                            exportOptions: {
                                columns: [0, 1, 2, 3,4,5,6]
                            }
                        },
                        {
                            extend: 'pdf',
                            exportOptions: {
                                columns: [0, 1, 2, 3,4,5,6]
                            }
                        },
                        {
                            extend: 'csv',
                            exportOptions: {
                                columns: [0, 1, 2, 3,4,5,6]
                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                columns: [0, 1, 2, 3,4,5,6]
                            }
                        },
                        'colvis'
                    ]
                });
            });
        </script>
    @endsection
