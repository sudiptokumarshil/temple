<div class="card-body table-responsive table-sm table-bordered">
    <br />
    <div class="row">
        @foreach ($books as $book)
            <div class="card" style="width: 18rem;">
                <p class="card-img-top"></p>
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <p class="card-text">{{ $book->item_name }}</p>
                    <input type="hidden" name="qty" id="qty-{{ $book->id }}" value="1" min="1" />
                    <button data-id='{{ $book->id }}' id="cartbtn" type="button" onclick="addToCart(this)"
                        class="col-12 btn border"><i class="fa fa-shopping-cart" style="color:blue"></i> &nbsp; Add to
                        Bag</button>
                </div>
            </div>
        @endforeach
    </div>
</div>
<script src='{{ asset('/') }}public/custom/bookstall.js'></script>
<script src='{{ asset('/') }}public/custom/api.js'></script>
