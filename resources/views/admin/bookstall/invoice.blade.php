@extends('admin.master')
@section('home')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Bookstall Invoice</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              <div class="row">
                <div class="col-12">
                  <h4>
                    <!--<i class="fas fa-globe"></i> Iskcon Nandankanan Chittagong,Bangladesh-->
                    <small class="float-right">Date:{{$invoice->date}} </small>
                  </h4>
                </div>
                <!-- /.col -->
              </div>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  From
                  <address>
                    <strong>'Sri Sri Radha Madhava Temple'</strong><br>
                    Chittagong,Bangladesh<br>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  To
                  <address>
                    <strong>{{$invoice->customer_name}}</strong><br>
                    Phone: {{$invoice->mobile_number}}<br>
                    Address: {{$invoice->address}}<br>
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>Invoice {{$invoice->id}}</b></br>
                  <br>
                  <b>Payment Due:</b> {{$invoice->due}}</br>
                  <b>Payment Paid:</b> {{$invoice->paid}} </br>
                  <b>Discount:</b> {{$invoice->discount}} </br>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- Table row -->
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                      <th>Serial</th>
                      <th>Item Name</th>
                      <th>Quantity</th>
                      <th>Unit Price</th>
                    </tr>
                    </thead>
                    <tbody>
                        @php($i=1)
                        @foreach($item as $v_inv)
                    <tr>
                      <td>{{$i++}}</td>
                      <td>{{$v_inv->item_name}}</td>
                      <td>{{$v_inv->qty}}</td>
                      <td>{{$v_inv->unit_price}}</td>
                    </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row">
                <!-- accepted payments column -->
                <div class="col-6">

                </div>
                <!-- /.col -->
                <div class="col-6">
                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th>Total:{{$invoice->price}}</th>
                        <td></td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <a href='{{route('bookstall-invoice-print',['id'=>$invoice->id])}}' target="_blank" class="btn btn-danger btn-sm"><i class="fas fa-print"></i> Print</a>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection
