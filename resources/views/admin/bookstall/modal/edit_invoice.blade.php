
<!-- Modal -->
<div class="modal fade" id="editInvoice" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header custom-card">
        <h5 class="modal-title" id="exampleModalLabel">Edit Invoice</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body card-body">
         <form>
          @csrf
            <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Total Amount:</label>
            <input disabled required name='total_amount' class="form-control total" id="total">
          </div>
        </div>
           <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Paid Amount:</label>
            <input type="hidden" required name='checkpaid' id="checkPaid" class="form-control"/>
            <input type="number" onkeyup='dueCount(this)' required name='paid' class="form-control" id="paid">
          </div>
        </div>
        <div class="card-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Due Amount:</label>
            <input type="number" disabled required name='due' class="form-control" id="due">
            <input type="hidden" id='invId'  name='id' class="form-control">
          </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
          <button type="button" onclick='updateInvoice(this)'  class="btn btn-success btn-sm">Update</button>
        </div>
      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script src='{{asset('/')}}public/custom/bookstall.js'></script>
<script src='{{asset('/')}}public/custom/api.js'></script>
<script>
 function dueCount(v){
        //   let total = $('#totalAmounts').val();
        let total = $('#total').val();

          let paid  = $('#paid').val();

          let totalValue = parseFloat(total) - parseFloat(paid);
          $('#due').val(totalValue)
        //   document.write(totalValue);
          console.log('hello',totalValue)
      }

    </script>
