<style>
    .myTable {
        width: 100%;
        text-align: left;
        background-color: lemonchiffon;
        border-collapse: collapse;
    }

    .myTable th {
        background-color: red;
        color: white;
    }

    .myTable td,
    .myTable th {
        padding: 10px;
        border: 1px solid green;
    }

    .blink {
        animation: blinker 4s linear infinite;
        color: yellow;
        font-size: 20px;
        font-weight: bold;
        font-family: sans-serif;
    }

    .custom-card {
        background-color: #318777;
        color: white;
        /* #17A2B8; */
    }

    .card-body {
        background-color: lemonchiffon;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

</style>
<div class="card-body table-responsive table-sm table-bordered">
    <table id="example" class="table myTable table-bordered table-hover display nowrap">
        <thead>
            <tr>
                <th>Sl</th>
                <th>Item Name</th>
                <th>Item Code</th>
                <th>Received Quantity</th>
                <th>Stock Available</th>
                <th>Sold</th>
                <th>Price</th>
                <th>Entry Date</th>
                <th>Action </th>
            </tr>
        </thead>
        <tbody>
            @php($i = 1)
                @foreach ($items as $item)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>
                            {{ $item->item_name }}
                        </td>
                        <td>
                            {{ $item->item_code }}
                        </td>

                        <td>
                            {{ $item->quantity }}
                        </td>
                        <td>
                            {{ $item->stock }}
                        </td>
                        <td>
                            {{ $item->sold }}
                        </td>
                        <td>
                            {{ $item->price }}
                        </td>
                        <td>
                            {{ date('Y-m-d', strtotime($item->created_at)) }}
                        </td>
                        <td>
                            {{-- <a href='{{ route('edit-book', ['id' => $item->id]) }}'
                                type='button' class='btn btn-success'>Edit</a> --}}
                            <a href='{{ route('itemstock-details', ['id' => $item->id]) }}' type='button'
                                class='btn btn-danger btn-sm'>Details</a>
                            {{-- <a href='{{ route('del-item', ['id' => $item->id]) }}'
                                type='button' class='btn btn-danger'>Delete</a> --}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
  <script>
    $(document).ready(function() {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [{
                    extend: 'copy',
                    exportOptions: {
                        columns: [0, 1, 2, 3,4,5,6,7]
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3,4,5,6,7]
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: [0, 1, 2, 3,4,5,6,7]
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [0, 1, 2, 3,4,5,6,7]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3,4,5,6,7]
                    }
                },
                'colvis'
            ]
        });
    });
</script>
