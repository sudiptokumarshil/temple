@extends('admin.master')
@section('home')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1></h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Stock Report Details</h3>

                            </div>
                            <!-- /.card-header -->
                            <h4 style='color:green' align='center'>{{ Session::get('message') }}</h4>

                            <div class="card-body table-responsive" id='itemData'>
                                @if ($item->item_name)
                                    <th>
                                        <h4 style="color:red" class="text-center">{{ $item->item_name }}</h4>
                                    </th>
                                @else
                                    <th>
                                        <h4 style="color:red" class="text-center">Not Found</h4>
                                    </th>
                                @endif
                                <table id="example" class="table table-bordered table-hover display nowrap">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>Entry Date</th>
                                            <th>Received Stock</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($i = 1)
                                            @php($totalStock = 0)
                                                @php($Stock = 0)

                                                    @foreach ($reports as $report)
                                                        <tr>
                                                            <td>{{ $i++ }}</td>
                                                            <td>
                                                                {{ date('d/M/Y', strtotime($report->created_at)) }}
                                                            </td>
                                                            <td>
                                                                {{ $report->quantity }}
                                                            </td>
                                                            <td>
                                                                {{ $report->price }}
                                                            </td>
                                                            <input type="hidden"
                                                                value="{{ $totalStock = $Stock += $report->quantity }}" />
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                                <tr>
                                                    <td></td>
                                                    <td>Stock Left:<h5 style="color:red">{{ $item->stock }}</h5>
                                                    </td>
                                                    <td>Total Stock:<h5 style="color:red">{{ $totalStock }}</h5>
                                                    </td>
                                                    <td>Total Sold:<h5 style="color:red">{{ $item->sold }}</h5>
                                                    </td>
                                                </tr>
                                            </table>

                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                    <!-- /.card -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->
                    </section>
                    <!-- /.content -->
                </div>
                <script src='{{ asset('/') }}public/custom/api.js'></script>
                <script src='{{ asset('/') }}public/custom/bookstall.js'></script>
            @endsection
