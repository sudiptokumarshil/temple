@extends('admin.master')
@section('home')
    <style>
        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: white;
        }
    </style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col--md-6">
                        <a href="{{ route('bookstall-salesform') }}" id="cart" class="logo-green mt-2 ml-2"><i
                                class="fa fa-shopping-cart logo-green"></i> Cart <span
                                class="badge bg-green cart_qty">{{ Cart::content()->count() }}</span></a>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">

                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div class='container'>
            <div class='row'>
                <div class='col-md-6'>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1"></label>
                        <input type='text' class='form-control' onkeyup='filterItem(this)'
                            placeholder='Enter Item Code Or Name' name='item_info' id='item_info'>
                    </div>
                </div>

                <div class='col-md-6'>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1"></label>
                        <!--<button type='button' style='margin-top:5%' onclick='filterItem(this)' align='center'  class='btn btn-success'>Search</button>-->
                    </div>
                </div>

            </div>
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <div class="card-title">
                                    All Bookstall Item
                                </div>
                            </div>

                            <div class="card-body" id='SaleItemData'>
                                <br />
                                <div class="row">
                                    @foreach (App\Models\BookstallItem::where('stock', '>', 0)->orderBy('id', 'desc')->cursor()
        as $book)
                                        <div class="card" style="width: 18rem;">
                                            <div class="card-body">
                                                <h5 class="card-title"></h5>
                                                <p class="card-text">{{ $book->item_name }}</p>
                                                <input type="hidden" name="qty" id="qty-{{ $book->id }}"
                                                    value="1" min="1" />
                                                <button data-id='{{ $book->id }}' id="cartbtn" type="button"
                                                    onclick="addToCart(this)" class="col-12 btn border"><i
                                                        class="fa fa-shopping-cart" style="color:blue"></i> &nbsp; Add to
                                                    Bag</button>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <script src='{{ asset('/') }}public/custom/bookstall.js'></script>
    <script src='{{ asset('/') }}public/custom/api.js'></script>
@endsection
