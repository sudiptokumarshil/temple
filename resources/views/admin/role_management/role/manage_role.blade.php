@extends('admin.master')
@section('home')
<style>
    .myTable {
        width: 100%;
        text-align: left;
        background-color: lemonchiffon;
        border-collapse: collapse;
    }

    .myTable th {
        background-color: red;
        color: white;
    }

    .myTable td,
    .myTable th {
        padding: 10px;
        border: 1px solid green;
    }

    .blink {
        animation: blinker 4s linear infinite;
        color: yellow;
        font-size: 20px;
        font-weight: bold;
        font-family: sans-serif;
    }

    .custom-card {
        background-color: #318777;
        color: white;
        /* #17A2B8; */
    }

    .card-body {
        background-color: lemonchiffon;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

</style>
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header custom-card">
                                <h3 class="card-title blink">All Role </h3>

                            </div>
                            <!-- /.card-header -->
                            <h4 style='color:green' align='center'>{{ Session::get('message') }}</h4>

                            <div class="card-body table-responsive myTable table-sm table-bordered" id='itemData'>
                                <a href='{{ route('add-role') }}' type='button' class='btn btn-success btn-sm'
                                    style='margin:10px'>
                                    Add role </a>
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Role Name</th>
                                            <th>Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($i = 1)
                                            @foreach ($allrole as $item)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $item->name }}</td>
                                                    <td>{{ $item->email }}</td>
                                                    @if ($item->role_id == 1)
                                                        <td><span style="color:red">Bookstall</span></td>
                                                    @elseif($item->role_id ==2)
                                                        <td><span style="color:red">Bakery</span></td>
                                                    @elseif($item->role_id ==3)
                                                        <td><span style="color:red">Admin</span></td>
                                                    @elseif($item->role_id ==4)
                                                        <td><span style="color:red">Nityaseva</span></td>
                                                    @elseif($item->role_id ==5)
                                                        <td><span style="color:red">Office</span></td>
                                                    @elseif($item->role_id ==6)
                                                        <td><span style="color:red">Bookstall & Bakery Both</span></td>
                                                    @elseif($item->role_id ==7)
                                                        <td><span style="color:red">Nityaseva & Office Both</span></td>
                                                    @elseif($item->role_id ==8)
                                                        <td><span style="color:red">Bakery Salesman</span></td>
                                                    @elseif($item->role_id ==9)
                                                        <td><span style="color:red">Bookstall Salesman</span></td>
                                                    @endif
                                                    {{-- @if ($item->status == 1)
                                            <td><span style="color: green">Active</span></td>
                                            @elseif($item->status ==2)
                                            <td><span style="color: green">Inactive</span></td>
                                            @endif --}}
                                                    <td>
                                                        <a href="{{ route('change-password', ['id' => $item->id]) }}"
                                                            type="button" class="btn btn-success btn-sm">Change Password</a>
                                                        <a href="{{ route('edit-role', ['id' => $item->id]) }}" type="button"
                                                            class="btn btn-danger btn-sm">Edit</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>

                                    </table>

                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
    @endsection
