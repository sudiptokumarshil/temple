@extends('admin.master')
@section('home')
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-8">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Add Role</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <div class="mt-4">
                                <form method="POST" action="{{ route('save-role') }}">
                                    @csrf

                                    <div class="form-group row">
                                        <label for="name"
                                            class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="name"
                                                value="{{ old('name') }}" required autocomplete="name" autofocus>

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label
                                            class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control " name="email"
                                                value="{{ old('email') }}" required autocomplete="email">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control" name="password"
                                                required autocomplete="new-password">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label
                                            class="col-md-4 col-form-label text-md-right">{{ __('Role And Permission') }}</label>

                                        <div class="col-md-6">
                                            <select id="role" class="form-control" name="role_id">
                                                <option value="1">Bookstall</option>
                                                <option value="2">Bakery </option>
                                                <option value="3">Admin</option>
                                                <option value="4">Nityaseva</option>
                                                <option value="5">Office</option>
                                                <option value="6">Bookstall & Bakery Both</option>
                                                <option value="7">Nityaseva &Office Both</option>
                                                <option value="8">Bakery Salesman</option>
                                                <option value="9">Bookstall Salesman</option>
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group row">
                                        <label class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>

                                        <div class="col-md-6">
                                            <select id="status" class="form-control" name="status">
                                                <option value="1">Active</option>
                                                <option value="2">Inactive </option>
                                            </select>
                                        </div>
                                    </div> --}}

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Register') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card -->
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
