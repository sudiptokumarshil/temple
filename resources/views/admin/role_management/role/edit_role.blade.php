@extends('admin.master')
@section('home')
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-8">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Update Role</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <div class="mt-4">
                                <form method="POST" action="{{ route('update-role') }}">
                                    @csrf

                                    <div class="form-group row">
                                        <label for="name"
                                            class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="name"
                                                value="{{$edit->name}}" required autocomplete="name" autofocus>
                                            <input id="id" type="hidden" name="id"
                                                value="{{$edit->id}}">

                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label
                                            class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email" class="form-control " name="email"
                                            value="{{$edit->email}}" required autocomplete="email">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label
                                            class="col-md-4 col-form-label text-md-right">{{ __('Role And Permission') }}</label>

                                        <div class="col-md-6">
                                            <select id="role" class="form-control" name="role_id">
                                                <option @if ($edit->role_id == 1) selected @endif value="1">Bookstall</option>
                                                <option @if ($edit->role_id == 2) selected @endif value="2">Bakery </option>
                                                <option @if ($edit->role_id == 3) selected @endif value="3">Admin</option>
                                                <option @if ($edit->role_id == 4) selected @endif value="4">Nityaseva</option>
                                                <option @if ($edit->role_id == 5) selected @endif value="5">Office</option>
                                                <option @if ($edit->role_id == 6) selected @endif value="6">Bookstall & Bakery Both</option>
                                                <option @if ($edit->role_id == 7) selected @endif value="7">Nityaseva &Office Both</option>
                                                <option @if ($edit->role_id == 8) selected @endif value="8">Bakery Salesman</option>
                                                <option @if ($edit->role_id == 9) selected @endif value="9">Bookstall Salesman</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Register') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /.card -->
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
