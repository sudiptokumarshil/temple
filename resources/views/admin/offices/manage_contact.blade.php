@extends('admin.master')
@section('home')
<style>
    .myTable {
        width: 100%;
        text-align: left;
        background-color: lemonchiffon;
        border-collapse: collapse;
    }

    .myTable th {
        background-color: red;
        color: white;
    }

    .myTable td,
    .myTable th {
        padding: 10px;
        border: 1px solid green;
    }

    .blink {
        animation: blinker 4s linear infinite;
        color: yellow;
        font-size: 20px;
        font-weight: bold;
        font-family: sans-serif;
    }

    .custom-card {
        background-color: #007BFF;
        color: white;
        /* #17A2B8; */
    }

    .card-body {
        background-color: lemonchiffon;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

</style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Offices</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card ">
                            <div class="card-header custom-card">
                                <h3 class="card-title"><b class="blink">All Member</b></h3>

                            </div>
                            <!-- /.card-header -->
                            <h5 class="text-center"><span style="color: green" >{{ Session::get('message') }}</span></h5>
                            <div class="card-body table-responsive table-sm table-bordered">
                                <a href='{{ route('add-contact') }}' type='button' class='btn btn-success btn-sm'
                                    style='margin:10px'> Add Contact</a>
                                    <table id="example" class="table myTable table-bordered table-hover display nowrap">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>Member Name</th>
                                            <th>Initiated Name</th>
                                            <th>Gotra</th>
                                            <th>Address</th>
                                            <th>Qualification</th>
                                            <th>Occupation</th>
                                            <th>Counsellor</th>
                                            <th>Age</th>
                                            <th>Department</th>
                                            <th>Date of Birth</th>
                                            <th>Festival Name</th>
                                            <th>Pranami</th>
                                            <th>Phone</th>
                                            <th>Photo</th>
                                            <th>Date</th>
                                            <th>Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($i = 1)
                                            @foreach ($alloffice as $v_alloffice)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $v_alloffice->name }}</td>
                                                    <td>{{ $v_alloffice->initiated_name }}</td>
                                                    <td>{{ $v_alloffice->goatra }}</td>
                                                    <td>{{ $v_alloffice->address }}</td>
                                                    <td>{{ $v_alloffice->qualification }}</td>
                                                    <td>{{ $v_alloffice->occupation }}</td>
                                                    <td>{{ $v_alloffice->collector }}</td>
                                                    <td>{{ $v_alloffice->age }}</td>
                                                    <td>{{ $v_alloffice->department }}</td>
                                                    <td>{{ $v_alloffice->date_of_birth }}</td>
                                                    <td>{{ $v_alloffice->festival_name }}</td>
                                                    <td>{{ $v_alloffice->pranami }}</td>
                                                    <td>{{ $v_alloffice->phone }}</td>
                                                    @if (!empty($v_alloffice->photo))
                                                        <td>
                                                            <img src="{{ asset($v_alloffice->photo) }}" width="50"
                                                                height="50" />
                                                        </td>
                                                    @else
                                                        <td>
                                                            <span style="color:red">
                                                                Not Found
                                                            </span>
                                                        </td>
                                                    @endif
                                                    <td>{{ $v_alloffice->date }}</td>
                                                    <td><a href="{{ route('edit-contact', ['id' => $v_alloffice->id]) }}"
                                                            class="btn btn-danger btn-sm">Edit</a></td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-end">

                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <script src='{{ asset('/') }}public/custom/api.js'></script>
        <script src='{{ asset('/') }}public/custom/nityaseva.js'></script>
    @endsection
