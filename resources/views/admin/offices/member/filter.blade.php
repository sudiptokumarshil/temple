<table id="example" class="table table-bordered table-responsive table-hover display nowrap">
    <thead>
        <tr>
            <th>Sl</th>
            <th>Member ID</th>
            <th>Member Name</th>
            <th>Gotra</th>
            <th>Address</th>
            <th>Qualification</th>
            <th>Occupation</th>
            <th>Counsellor</th>
            <th>Blood Group</th>
            <th>Age</th>
            <th>Department</th>
            <th>Date of Birth</th>
            <th>Phone</th>
            <th>Pranami</th>
            <th>Photo</th>
            <th>Action </th>
        </tr>
    </thead>
    <tbody>
        @php($i = 1)
            @foreach ($customers as $customer)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>
                        {{ $customer->member_id }}
                    </td>
                    <td>
                        {{ $customer->name }}
                    </td>
                    <td>
                        {{ $customer->gotra }}
                    </td>
                    <td>
                        {{ $customer->address }}
                    </td>
                    <td>
                        {{ $customer->qualification }}
                    </td>
                    <td>
                        {{ $customer->occupation }}
                    </td>

                    <td>
                        {{ $customer->counsellor }}
                    </td>
                    <td>
                        {{ $customer->blood_group }}
                    </td>
                    <td>
                        {{ $customer->age }}
                    </td>
                    <td>
                        {{ $customer->department }}
                    </td>
                    <td>
                        {{ $customer->date_of_birth }}
                    </td>
                    <td>
                        {{ $customer->phone }}
                    </td>
                    <td>
                        {{ $customer->pranami }}
                    </td>
                    @if (!empty($customer->customer_image))
                        <td>
                            <img src="{{ asset($customer->customer_image) }}" width="50"
                                height="50" />
                        </td>
                    @else
                        <td>
                            <span style="color:red">
                                Not Found
                            </span>
                        </td>
                    @endif
                    <td>
                        <!-- Button trigger modal -->
                        <a href="{{route('edit-office-member',['id'=>$customer->id])}}" type="button"
                            class="btn btn-primary btn-sm">
                            Edit
                    </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
