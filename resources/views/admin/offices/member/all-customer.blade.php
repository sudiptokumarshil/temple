@extends('admin.master')
@section('home')
<style>
    .myTable {
        width: 100%;
        text-align: left;
        background-color: lemonchiffon;
        border-collapse: collapse;
    }

    .myTable th {
        background-color: red;
        color: white;
    }

    .myTable td,
    .myTable th {
        padding: 10px;
        border: 1px solid green;
    }

    .blink {
        animation: blinker 4s linear infinite;
        color: yellow;
        font-size: 20px;
        font-weight: bold;
        font-family: sans-serif;
    }

    .custom-card {
        background-color: #007BFF;
        color: white;
        /* #17A2B8; */
    }

    .card-body {
        background-color: lemonchiffon;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

</style>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1></h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item active"></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header custom-card">
                                <h3 class="card-title"><b class="blink">All Office Member</b></h3>
                            </div>
                            <!-- /.card-header -->
                            <h5 class="text-center"><span style="color: green">{{Session::get('message')}}</span></h5>
                            <div class="card-body myTable table-responsive table-sm table-bordered">
                                <div class='row'>
                                    <div class='col-md-4'>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">Member Name</label>
                                            <input type='text' class='form-control customer_info input-sm' name='name'
                                                id='Name'>
                                        </div>
                                    </div>

                                    <div class='col-md-4'>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">counsellor</label>
                                            <input type='text' class='form-control input-sm' name='counsellor' id='Counsellor'>
                                        </div>
                                    </div>
                                    <div class='col-md-4'>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect1">department</label>
                                            <input type='text' class='form-control input-sm' name='department' id='Department'>
                                        </div>
                                    </div>
                                    <div class='col-md-4'>
                                        <div class="form-group">
                                        </div>
                                    </div>
                                    <div class='col-md-4'>
                                        <div class="form-group">
                                            <button type='button' style="margin-left:30%" onclick='report()'
                                                class='btn btn-success btn-sm'>Search</button>
                                        </div>
                                    </div>
                                    <div class='col-md-4'>
                                        <div class="form-group">
                                        </div>
                                    </div>
                                </div>
                                <a href='{{ route('create-office-member') }}' type='button' class='btn btn-success btn-sm'
                                    style='margin:10px'> Add Member </a>
                                    <table id="example" class="table table-bordered table-responsive table-hover display nowrap">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>Member ID</th>
                                            <th>Member Name</th>
                                            <th>Gotra</th>
                                            <th>Address</th>
                                            <th>Qualification</th>
                                            <th>Occupation</th>
                                            <th>Counsellor</th>
                                            <th>Blood Group</th>
                                            <th>Age</th>
                                            <th>Department</th>
                                            <th>Date of Birth</th>
                                            <th>Phone</th>
                                            <th>Pranami</th>
                                            <th>Photo</th>
                                            <th>Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($i = 1)
                                            @foreach ($customers as $customer)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>
                                                        {{ $customer->member_id }}
                                                    </td>
                                                    <td>
                                                        {{ $customer->name }}
                                                    </td>
                                                    <td>
                                                        {{ $customer->gotra }}
                                                    </td>
                                                    <td>
                                                        {{ $customer->address }}
                                                    </td>
                                                    <td>
                                                        {{ $customer->qualification }}
                                                    </td>
                                                    <td>
                                                        {{ $customer->occupation }}
                                                    </td>

                                                    <td>
                                                        {{ $customer->counsellor }}
                                                    </td>
                                                    <td>
                                                        {{ $customer->blood_group }}
                                                    </td>
                                                    <td>
                                                        {{ $customer->age }}
                                                    </td>
                                                    <td>
                                                        {{ $customer->department }}
                                                    </td>
                                                    <td>
                                                        {{ $customer->date_of_birth }}
                                                    </td>
                                                    <td>
                                                        {{ $customer->phone }}
                                                    </td>
                                                    <td>
                                                        {{ $customer->pranami }}
                                                    </td>
                                                    @if (!empty($customer->customer_image))
                                                        <td>
                                                            <img src="{{ asset($customer->customer_image) }}" width="50"
                                                                height="50" />
                                                        </td>
                                                    @else
                                                        <td>
                                                            <span style="color:red">
                                                                Not Found
                                                            </span>
                                                        </td>
                                                    @endif
                                                    <td>
                                                        <!-- Button trigger modal -->
                                                        <a href="{{route('edit-office-member',['id'=>$customer->id])}}" type="button"
                                                            class="btn btn-primary btn-sm">
                                                            Edit
                                                    </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-end">
                                        {!! $customers->links() !!}
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <script src='{{ asset('/') }}public/custom/api.js'></script>
        <script src='{{ asset('/') }}public/custom/office.js'></script>
    @endsection
