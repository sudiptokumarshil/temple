@extends('admin.master')
@section('home')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                    <div>{{ $error }}</div>
                                @endforeach
                            @endif
                            <div class="card-header">
                                <h3 class="card-title">Add Office Member</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="{{ route('store-office-member') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Member ID</label>
                                        <input type="text" required name='member_id' class="form-control" id="member_id"
                                            placeholder="Enter Member ID">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Member Name</label>
                                        <input type="text" required name='name' class="form-control" id="name"
                                            placeholder="Enter Member Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Gotra</label>
                                        <input type="text" required name='gotra' class="form-control" id="gotra"
                                            placeholder="Enter Gotra">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Address</label>
                                        <textarea name='address' class="form-control" id="address"
                                            placeholder="Enter Address"></textarea>
                                    </div>


                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Qualification</label>
                                        <textarea name='qualification' class="form-control" id="qualification"
                                            placeholder="Enter Qualification"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Occupation</label>
                                        <textarea name='occupation' class="form-control" id="occupation"
                                            placeholder="Enter Occupation"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Counsellor</label>
                                        <textarea name='counsellor' class="form-control" id="counsellor"
                                            placeholder="Enter Counsellor"></textarea>
                                    </div>


                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Blood Group</label>
                                        <input type="text" required name='blood_group' class="form-control" id="blood_group"
                                            placeholder="Enter Blood Group">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Age </label>
                                        <input type="number" required name='age' class="form-control" id="age"
                                            placeholder="Enter Age">
                                    </div>


                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Department </label>
                                        <input type="text" required name='department' class="form-control" id="department"
                                            placeholder="Enter Department Name">
                                    </div>


                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Birth Date </label>
                                        <input type="date" required name='date_of_birth' class="form-control"
                                            id="date_of_birth" placeholder="Enter Birth Date">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Phone </label>
                                        <input type="text" required name='phone' class="form-control" id="phone"
                                            placeholder="Enter Phone Number ">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Pranami </label>
                                        <input type="number" required name='pranami' class="form-control" id="pranami"
                                            placeholder="Enter Amount Of Pranami ">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Photo</label>
                                        <input  id="file-upload" type="file" name="customer_image" />
                                        <img id="blah" src="#" alt="" style="height:100px; border: 2px solid grey" />
                                    </div>
                                </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    </form>
                </div>
                <!-- /.card -->
                <!-- /.card -->
            </div>
            <!--/.col (right) -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <script src='{{ asset('/') }}public/custom/api.js'></script>
    <script src='{{ asset('/') }}public/custom/nityaseva.js'></script>
    <script>
        $('#blah').hide();

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
            $('#blah').show();
        }
        $("#file-upload").change(function() {
            readURL(this);
        });

    </script>
@endsection
