@extends('admin.master')
@section('home')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                    <div>{{ $error }}</div>
                                @endforeach
                            @endif
                            <div class="card-header">
                                <h3 class="card-title">Edit Contact</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="{{ route('update-contact') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Member Name</label>
                                        <input type="text" value="{{$edit->name}}" required name='name' class="form-control" id="name"
                                            placeholder="Enter Customer Name">
                                        <input type="hidden" value="{{$edit->id}}" name='id'>
                                    </div>
                                     <div class="form-group">
                                        <label for="exampleInputEmail1">Initiated Name</label>
                                        <input type="text" required value="{{$edit->initiated_name}}" name='initiated_name' class="form-control" id="initiated_name"
                                            placeholder="Enter Customer Name"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Gotra</label>
                                        <input type="text" value="{{$edit->goatra}}" required name='goatra' class="form-control" id="gotra"
                                            placeholder="Enter Gotra">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Festival Name</label>
                                        <input type="text" value="{{$edit->festival_name}}" required name='festival_name' class="form-control" id="festival_name"
                                            placeholder="Enter Gotra">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Address</label>
                                        <textarea name='address' class="form-control" id="address"
                                            placeholder="Enter Address">{{$edit->address}}</textarea>
                                    </div>


                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Qualification</label>
                                        <textarea name='qualification'  class="form-control" id="qualification"
                                            placeholder="Enter Qualification">{{$edit->qualification}}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Occupation</label>
                                        <textarea name='occupation' class="form-control" id="occupation"
                                            placeholder="Enter Occupation">{{$edit->occupation}}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Counsellor</label>
                                        <textarea name='collector' class="form-control" id="collector"
                                            placeholder="Enter Counsellor">{{$edit->collector}}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Age </label>
                                        <input type="number" required name='age' value="{{$edit->age}}" class="form-control" id="age"
                                            placeholder="Enter Age">
                                    </div>


                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Department </label>
                                        <input type="text" required name='department' value="{{$edit->department}}" class="form-control" id="department"
                                            placeholder="Enter Department Name">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Pranami  </label>
                                        <input type="number" required name='pranami' value="{{$edit->pranami}}" class="form-control" id="department"
                                            placeholder="Enter Department Name">
                                    </div>


                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Birth Date </label>
                                        <input type="date" required name='date_of_birth' value="{{$edit->date_of_birth}}" class="form-control"
                                            id="date_of_birth" placeholder="Enter Birth Date">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Phone </label>
                                        <input type="text" required name='phone' value="{{$edit->phone}}" class="form-control" id="phone"
                                            placeholder="Enter Phone Number ">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date </label>
                                        <input type="date" required value="{{$edit->date}}" name='date' class="form-control" id="date"/>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Photo</label>
                                        <input  id="file-upload" type="file" name="photo" />
                                        <img id="blah" src="#" alt="" style="height:100px; border: 2px solid grey" />
                                        <div>
                                            <img src="{{asset($edit->photo)}}" width="100" height="100"/>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                    </form>
                </div>
                <!-- /.card -->
                <!-- /.card -->
            </div>
            <!--/.col (right) -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
    <script src='{{ asset('/') }}public/custom/api.js'></script>
    <script src='{{ asset('/') }}public/custom/nityaseva.js'></script>
    <script>
        $('#blah').hide();

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
            $('#blah').show();
        }
        $("#file-upload").change(function() {
            readURL(this);
        });

    </script>
@endsection
