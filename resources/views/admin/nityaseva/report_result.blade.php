<div class="card-body table-responsive table-sm table-bordered">
    <table id="example" class="table table-bordered table-hover display nowrap">
        <thead>
            <tr>
                <th>Sl</th>
                <th>Member Name</th>
                <th>Member Phone</th>
                <th>Amount of Pranami</th>
                <th>Date</th>
                <th>Action </th>
            </tr>
        </thead>
        <tbody>
            @php($i = 1)
            @php($totalAmount =0)
                @foreach ($report as $b_report)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>
                            {{ $b_report->customer_name }}
                        </td>
                        <td>
                            {{ $b_report->customer_phone }}
                        </td>
                        <td>
                            {{ $b_report->pranami }}
                        </td>
                        <td>
                            {{ $b_report->date }}
                            <input type="hidden" value="{{ $totalAmount = $totalAmount += $b_report->pranami  }}" />
                        </td>

                        <td>
                            <button type="button" class="btn btn-primary btn-sm"
                                onclick="editPranami({{ $b_report->id }})" data-toggle="modal"
                                data-target="#exampleEditPranamiModal">
                                Edit
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <p style="color:red; display-inline-block">Total Amount</p>
                    {{ $totalAmount}}
                </td>
                <td>

                </td>
                <td></td>
            </tr>
        </table>
    </div>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [{
                    extend: 'copy',
                    exportOptions: {
                        columns: [0, 1, 2, 3,4]
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3,4]
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: [0, 1, 2, 3,4]
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [0, 1, 2, 3,4]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3,4]
                    }
                },
                'colvis'
            ]
        });
    });
</script>
