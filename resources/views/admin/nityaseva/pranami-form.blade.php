@extends('admin.master')
@section('home')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Pranami</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form>
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Member Name</label>
                    <select class="form-control js-example-basic-single" name='customer_id' id="customer_id">
                        <option value='0'>{{'Choose One'}}</option>
                        @foreach($customers as $customer)
                         <option value='{{$customer->id}}'>{{$customer->name}}</option>
                        @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Pranami</label>
                    <input type="text" required name='pranami' class="form-control" id="pranami" placeholder="Enter Amount Of Pranami">
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Date</label>
                    <input type="date" required name='date' class="form-control" id="date">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button onclick='addPranami(this)' type="button" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script src='{{asset('/')}}public/custom/api.js'></script>
  <script src='{{asset('/')}}public/custom/nityaseva.js'></script>
@endsection
