<!-- Modal -->
<div class="modal fade" id="exampleEditPranamiModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Pranami Form</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Member Name</label>
                            <select class="form-control js-example-basic-single customer_id" style="width:100%" name='customer_id' id="customer_id">
                                <option value='0'>{{ 'Choose One' }}</option>
                                @foreach ($customers as $customer)
                                    <option value='{{ $customer->id }}'>{{ $customer->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Pranami</label>
                            <input type="text" required name='pranami' class="form-control" id="pranami"
                                placeholder="Enter Amount Of Pranami">
                            <input type="hidden" name='id' id="pranami_ids">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Date</label>
                            <input type="date" required name='date' class="form-control" id="date">
                        </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button onclick='addPranami(this)' type="button" class="btn btn-primary btn-sm">Save</button>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
