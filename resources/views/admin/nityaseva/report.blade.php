@extends('admin.master')
@section('home')
<style>
    .myTable {
        width: 100%;
        text-align: left;
        background-color: lemonchiffon;
        border-collapse: collapse;
    }

    .myTable th {
        background-color: red;
        color: white;
    }

    .myTable td,
    .myTable th {
        padding: 10px;
        border: 1px solid green;
    }

    .blink {
        animation: blinker 4s linear infinite;
        color: yellow;
        font-size: 20px;
        font-weight: bold;
        font-family: sans-serif;
    }

    .custom-card {
        background-color: #318777;
        color: white;
        /* #17A2B8; */
    }

    .card-body {
        background-color: lemonchiffon;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }

</style>
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header custom-card ">
                       <b class="blink"> Pranami Report</b>
                    </div>
                    <div class="card-body">
                        <div class='row'>
                            <div class='col-md-4'>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Member Name</label>
                                    <select class="form-control js-example-basic-single" width="100%" name='customer_id' id='customer_ids'>
                                        <option value='0'>Choose One</option>
                                        @foreach ($customers as $customer)
                                            <option value='{{ $customer->id }}'>{{ $customer->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class='col-md-4'>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Start Date</label>
                                    <input type='date' class='form-control input-sm' style="line-height: 28px;" name='start_date' id='start_date'>
                                </div>
                            </div>
                            <div class='col-md-4'>
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">End Date</label>
                                    <input type='date' class='form-control input-sm' name='end_date' id='end_date'>
                                </div>
                            </div>
                            <div class='col-md-4'>
                                <div class="form-group">

                                </div>
                            </div>
                            <div class='col-md-4'>
                                <div class="form-group text-center">
                                    <button type='button' onclick='pranami_report(this)' align='center'
                                            class='btn btn-success btn-sm'>Search</button>
                                </div>
                            </div>
                            <div class='col-md-4'>
                                <div class="form-group">

                                </div>
                            </div>
                        </div>
                    </div>
                  </div>

            </div>
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header custom-card">
                                <h3 class="card-title"><b class="blink">All Pranami Report</b></h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive myTable table-sm table-bordered" id='reportResult'>
                                <table id="example"
                                    class="table table-bordered table-hover display nowrap">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>Member Name</th>
                                            <th>Member Phone</th>
                                            <th>Amount Of Pranami</th>
                                            <th>Date</th>
                                            <th>Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($i = 1)
                                        @php($totalAmount =0)
                                            @foreach ($pranami as $v_pranami)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>
                                                        {{ $v_pranami->customer_name }}
                                                    </td>
                                                    <td>
                                                        {{ $v_pranami->customer_phone }}
                                                    </td>
                                                    <td>
                                                        {{ $v_pranami->pranami }}
                                                    </td>
                                                    <td>
                                                        {{ $v_pranami->date }}
                                                        <input type="hidden" value="{{ $totalAmount = $totalAmount += $v_pranami->pranami  }}" />
                                                    </td>
                                                    <td>
                                                        <!-- Button trigger modal -->
                                                        <button type="button" class="btn btn-primary btn-sm"
                                                            onclick="editPranami({{ $v_pranami->id }})" data-toggle="modal"
                                                            data-target="#exampleEditPranamiModal">
                                                            Edit
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <p style="color:red; display-inline-block">Total Amount</p>
                                                {{ $totalAmount}}
                                            </td>
                                            <td>

                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                    <div class="d-flex justify-content-end">
                                        {!! $pranami->links() !!}
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <script src='{{ asset('/') }}public/custom/api.js'></script>
        <script src='{{ asset('/') }}public/custom/nityaseva.js'></script>
        @include('admin.nityaseva.edit-pranami-form')
        <script>
            $(document).ready(function() {
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [{
                            extend: 'copy',
                            exportOptions: {
                                columns: [0, 1, 2, 3,4]
                            }
                        },
                        {
                            extend: 'excel',
                            exportOptions: {
                                columns: [0, 1, 2, 3,4]
                            }
                        },
                        {
                            extend: 'pdf',
                            exportOptions: {
                                columns: [0, 1, 2, 3,4]
                            }
                        },
                        {
                            extend: 'csv',
                            exportOptions: {
                                columns: [0, 1, 2, 3,4]
                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                columns: [0, 1, 2, 3,4]
                            }
                        },
                        'colvis'
                    ]
                });
            });
        </script>
    @endsection
