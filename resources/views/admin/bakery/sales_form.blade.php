@extends('admin.master')
@section('home')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Bakery Sales Form</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <!--<li class="breadcrumb-item active"></li>-->
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">All Item</h3>

                            </div>
                            <!-- /.card-header -->

                            <div class="card-body table-responsive table-sm table-bordered">
                                {{-- <a href='{{route('add-book')}}' type='button' class='btn btn-success' style='margin:10px'> Add Book </a> --}}
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Sl</th>
                                            <th>Name</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Total Price</th>
                                            <th>Action </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($i = 1)
                                        @php($subtotal = 0)
                                        @php($tot = 0)
                                        @php($grossTotal = 0)

                                        @foreach ($cart as $item)
                                            <tr class="tr-{{ $item->rowId }}">
                                                <td>{{ $i++ }}</td>
                                                <td>
                                                    {{ $item->name }}
                                                </td>

                                                <td>

                                                    <input type='number' readonly id="qty-{{ $item->rowId }}" name="qty"
                                                        class='form-control' size="1" value='{{ $item->qty }}'>
                                                    <input type="hidden" class="form-control text-center"
                                                        id="rowId-{{ $item->rowId }}" name="rowId"
                                                        value="{{ $item->rowId }}">
                                                    <input type="hidden" class="form-control text-center"
                                                        id="itemIid-{{ $item->rowId }}" name="item_id"
                                                        value="{{ $item->id }}">

                                                </td>
                                                <td>
                                                    {{ $item->price }}
                                                </td>
                                                <td id="subtotal" class="text-center amount-<?php echo $item->rowId; ?>">
                                                    {{ $tot = $item->price * $item->qty }}
                                                    BDT
                                                </td>
                                                <td>
                                                    <button data-id="{{ $item->rowId }}" type="button"
                                                        class="quantity-left-minus btn btn-danger btn-sm btn_loading btn-number"
                                                        data-type="minus" id="decrease" onclick="decreaseValue(this)"
                                                        data-field="">
                                                        <i class="fa fa-minus"></i>
                                                        <span class="cart_loading"></span>
                                                    </button>
                                                    <button data-id="{{ $item->rowId }}" type="button"
                                                        class="quantity-right-plus btn btn-success btn-sm btn_loading btn-number"
                                                        data-type="plus" id="increase" onclick="increaseValue(this)"
                                                        data-field="">
                                                        <i class="fa fa-plus"></i>
                                                        <span class="cart_loading"></span>

                                                    </button>
                                                    <button data-id="{{ $item->rowId }}" type="button"
                                                        class="btn btn-danger btn-sm btn-sm btn_loading"
                                                        onclick="deleteCart(this)">
                                                        <i class="fa fa-trash"></i>
                                                        <span class="cart_loading"></span>
                                                    </button>
                                                </td>
                                                <input type='hidden' class='here' disabled
                                                    value='{{ $grossTotal = $subtotal += $tot }}'>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <span class="bg-blue text-white drop-shadow"> Total :
                                    <input type='number' disabled class='final_amount_before' id="totalAmounts"
                                        value='{{ $grossTotal }}'>
                                </span>
                                <span class="bg-blue text-white drop-shadow" style="margin-left: 45%"> Total Payable :
                                    <input type='number' disabled class='text-right final_amount_before' name='total_price'
                                        id="netPayable" value='{{ $grossTotal }}'>
                                </span>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <!--</div>-->
            <!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Add Information </h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Customer Name</label>
                                        <input type="text" required name='customer_name' class="form-control"
                                            id="customer_name" placeholder="Enter Customer Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Mobile Number</label>
                                        <input type="text" required name='mobile_number' class="form-control"
                                            id="mobile_number" placeholder="Enter Mobile Number">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Address</label>
                                        <input type="text" required name='address' class="form-control" id="address"
                                            placeholder="Enter Address">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Discount(taka)</label>
                                        <input type="number" required name='discount' class="form-control discount"
                                            id="discount" onkeyup='discounts(this)'
                                            placeholder="Taka Only No Percentage Allowed">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Paid</label>
                                        <input type="number" required name='paid' class="form-control paid" id="paid"
                                            onkeyup='dueCount(this)' placeholder="Enter Paid Or Not">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Due</label>
                                        <input type="number" readonly name='due' class="form-control due" id="due"
                                            placeholder="Enter Due Or Not">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Date</label>
                                        <input type="date" required name='date' class="form-control" id="date">
                                    </div>
                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button onclick='orderCheckout(this)' type="button"
                                        class="btn btn-primary btn-loader btn-sm" id="bakerySubmitButton">Continue</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                        <!-- /.card -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <script src='{{ asset('/') }}public/custom/bakery.js'></script>
    <script src='{{ asset('/') }}public/custom/api.js'></script>

    <script type="text/javascript">
        function discounts() {
            let discount = $('.discount').val();
            let total = $('#totalAmounts').val();
            if (typeof discount == 'undefined' || discount == '') {
                discount = 0;
            }
            if (Number(total) > Number(discount)) {
                let totalAmount = parseFloat(total) - parseFloat(discount);
                $('#netPayable').val(totalAmount);
                dueCount(1);
                $("#bakerySubmitButton").prop("disabled", false);
            } else {
                $("#bakerySubmitButton").prop("disabled", true);
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Discount cannot be gretter or equal to total amount!!',
                })
            }


        }

        function dueCount(v) {
            let total = $('#netPayable').val();
            let paid = $('.paid').val();
            if (typeof paid == 'undefined' || paid == '') {
                paid = 0;
            }
            let totalValue = parseFloat(total) - parseFloat(paid);
            $('.due').val(totalValue)
        }
    </script>

@endsection
