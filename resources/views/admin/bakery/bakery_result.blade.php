<div class="card-body table-responsive table-sm table-bordered">
    <table id="example2" class="display myTable datatable table-responsive w-100 d-block d-md-table table table-stripped table-bordered mt-3">
        <thead>
            <tr>
                <th>Sl</th>
                <th>Item Name</th>
                <th>Item Code</th>
                <th>Received Quantity</th>
                <th>Stock</th>
                <th>Price</th>
                <th>Action </th>
            </tr>
        </thead>
        <tbody>
            @php($i = 1)
                @foreach ($items as $item)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>
                            {{ $item->item_name }}
                        </td>
                        <td>
                            {{ $item->item_code }}
                        </td>

                        <td>
                            {{ $item->quantity }}
                        </td>
                        <td>
                            {{ $item->stock }}
                        </td>
                        <td>
                            {{ $item->price }}
                        </td>
                        <td>
                            <a href='{{ route('edit-item', ['id' => $item->id]) }}' type='button'
                                class='btn btn-success btn-sm'>Edit</a>
                            {{-- <a href='{{ route('del-item', ['id' => $item->id]) }}'
                                type='button' class='btn btn-danger'>Delete</a> --}}

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
