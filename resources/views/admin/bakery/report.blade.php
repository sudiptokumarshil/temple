@extends('admin.master')
@section('home')
    <div class="content-wrapper">
        <style>
            .myTable {
                width: 100%;
                text-align: left;
                background-color: lemonchiffon;
                border-collapse: collapse;
            }

            .myTable th {
                background-color: red;
                color: white;
            }

            .myTable td,
            .myTable th {
                padding: 10px;
                border: 1px solid green;
            }

            .blink {
                animation: blinker 4s linear infinite;
                color: yellow;
                font-size: 20px;
                font-weight: bold;
                font-family: sans-serif;
            }

            .custom-card {
                background-color: #318777;
                color: white;
                /* #17A2B8; */
            }

            .card-body {
                background-color: lemonchiffon;
            }

            @keyframes blinker {
                50% {
                    opacity: 0;
                }
            }

        </style>
        <div class="row">
            <div class="col-xs-12">
                <h4 class="page-title"> </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header custom-card text-center"><b class="blink">Bakery Sales Report</b></div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class='col-md-4'>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Customer Name</label>
                                        <input type='text' class='form-control customer_info' name='customer_name'
                                            id='customer_name'>
                                    </div>
                                </div>

                                <div class='col-md-4'>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Start Date</label>
                                        <input type='date' class='form-control' name='start_date' id='start_date'>
                                    </div>
                                </div>
                                <div class='col-md-4'>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">End Date</label>
                                        <input type='date' class='form-control' name='end_date' id='end_date'>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class='col-md-12'>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        <label for="exampleFormControlSelect1"></label>
                                        <button type='button' onclick='report(this)'
                                            class='btn btn-success btn-sm'>Search</button>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                        <div class='text-right'>

                        </div>
                        <p id="dateMessage" style="color: red" class="text-center"></p>
                        <div id="reportResult">
                            <table id="example"
                                class="display myTable datatable table-responsive w-100 d-block d-md-table table table-stripped table-bordered mt-3">
                                <thead>
                                    <tr>
                                        <th>Sl</th>
                                        <th>Customer Name</th>
                                        <th>Customer Phone</th>
                                        <th>Paid</th>
                                        <th>Due</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Updated By</th>
                                        <th>Sale Date</th>
                                        <th>Updated Date</th>
                                        <th>Action </th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @php($i = 1)
                                    @php($allpaid = 0)
                                    @php($totalPaid = 0)
                                    @php($allDue = 0)
                                    @php($totalDue = 0)
                                    @foreach ($reports as $item)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>
                                                {{ $item->customer_name }}
                                            </td>
                                            <td>
                                                {{ $item->phone }}
                                            </td>
                                            <td>
                                                {{ $item->paid }}
                                            </td>
                                            <td>
                                                {{ $item->due }}
                                            </td>
                                            <td>
                                                {{ $item->quantity }}
                                            </td>
                                            <td>
                                                {{ $item->price }}
                                            </td>
                                            @if ($item->user_name)
                                                <td>
                                                    {{ $item->user_name }}
                                                </td>
                                            @else
                                                <td>
                                                    <p style="color:red">Not Updated Yet</p>
                                                </td>
                                            @endif
                                            <td>
                                                {{ $item->date }}
                                            </td>
                                            <td>
                                                {{ $item->updated_at }}
                                            </td>
                                            <td>
                                                <a href='{{ route('bakery-invoice', ['id' => $item->id]) }}'
                                                    type='button' class='btn btn-danger btn-sm'>Details</a>
                                                <input type="hidden"
                                                    value="{{ $totalPaid = $allpaid += $item->paid }}" />
                                                <input type="hidden" value="{{ $totalDue = $allDue += $item->due }}" />
                                                <!-- Button trigger modal -->
                                                @if ($item->due > 0)
                                                    <button type="button" onclick='editInvoice({{ $item->id }})'
                                                        class="btn btn-primary btn-sm" data-toggle="modal"
                                                        data-target="#editInvoice">
                                                        Edit
                                                    </button>
                                                @else

                                                @endif

                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <p style="color:red; display-inline-block">Total Paid</p>
                                            {{ $totalPaid }}
                                        </td>
                                        <td>
                                            <p style="color:red">Total Due</p> {{ $totalDue }}
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>

                                {!! $reports->links() !!}
                            </table>
                        </div>
                        <div class="d-flex justify-content-end">
                            {{-- {{ $addCharges->links() }} --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
        <!-- /.content -->
    </div>
    @include('admin.bakery.modal.edit_invoice')
    <script src='{{ asset('/') }}public/custom/api.js'></script>
    <script src='{{ asset('/') }}public/custom/bakery.js'></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'copy',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                        }
                    },
                    {
                        extend: 'excel',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                        }
                    },
                    {
                        extend: 'pdf',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                        }
                    },
                    {
                        extend: 'csv',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                        }
                    },
                    {
                        extend: 'print',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                        }
                    },
                    'colvis'
                ]
            });
        });
    </script>

@endsection
