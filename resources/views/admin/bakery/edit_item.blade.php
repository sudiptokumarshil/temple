@extends('admin.master')
@section('home')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Update Item</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form>
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Item Code</label>
                    <input type="text" required name='item_code' class="form-control" value='{{$item->item_code}}' id="item_code" placeholder="Enter Item Code">
                    <input type="hidden"  name='id' class="form-control" value='{{$item->id}}' id="item_id" placeholder="Enter Item Code">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Item Name</label>
                    <input type="text" required name='item_name' class="form-control" value='{{$item->item_name}}' id="item_name" placeholder="Enter Item Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Item Quantity</label>
                    <input type="number" required name='quantity' class="form-control" value='{{$item->quantity}}' id="quantity" placeholder="Enter Item Quantity">
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Item Price</label>
                    <input type="number" required name='price' class="form-control" value='{{$item->price}}' id="price" placeholder="Enter Item Price">
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Date</label>
                    <input type="date" required name='date' class="form-control" value='{{$item->date}}' id="date" placeholder="Enter Item Quantity">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button onclick='updateItem(this)' type="button" class="btn btn-primary">Update</button>
                   <a href='{{route('all-item')}}' type='button' class='btn btn-success' style='margin:10px'> All Item </a>
                </div>
              </form>
            </div>
            <!-- /.card -->
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script src='{{asset('/')}}public/custom/api.js'></script>
  <script src='{{asset('/')}}public/custom/bakery.js'></script>
@endsection