@extends('admin.master')
@section('home')

    <div class="content-wrapper">
        <style>
            .myTable {
                width: 100%;
                text-align: left;
                background-color: lemonchiffon;
                border-collapse: collapse;
            }

            .myTable th {
                background-color: red;
                color: white;
            }

            .myTable td,
            .myTable th {
                padding: 10px;
                border: 1px solid green;
            }

            .blink {
                animation: blinker 4s linear infinite;
                color: yellow;
                font-size: 20px;
                font-weight: bold;
                font-family: sans-serif;
            }

            .custom-card {
                background-color: #318777;
                color: white;
                /* #17A2B8; */
            }

            .card-body {
                background-color: lemonchiffon;
            }

            @keyframes blinker {
                50% {
                    opacity: 0;
                }
            }

        </style>
        <div class="row">
            <div class="col-xs-12">
                <h4 class="page-title"> </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header custom-card text-center"><b class="blink">Bakery All Item</b></div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type='text' class='form-control' onkeyup='searchItem(this)'
                                        placeholder='Enter Item Code Or Name' name='item_info' id='item_info' />
                                </div>
                            </div>
                        </div>
                        <div class='text-right'>
                            <a href='{{ route('add-item') }}' type='button' class='btn btn-success btn-sm'
                                style='margin:10px'>
                                Add New Item </a>
                        </div>
                        <h4 style='color:green' align='center'>{{ Session::get('message') }}</h4>
                        <div class="card-body table-responsive table-sm table-bordered" id='itemData'>
                            <table id="example"
                                class="display myTable datatable table-responsive w-100 d-block d-md-table table table-stripped table-bordered mt-3">
                                <thead>
                                    <tr>
                                        <th>Sl</th>
                                        <th>Item Name</th>
                                        <th>Item Code</th>
                                        <th>Received Quantity</th>
                                        <th>Stock</th>
                                        <th>Price</th>
                                        <th>Action </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php($i = 1)
                                    @foreach ($items as $item)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>
                                                {{ $item->item_name }}
                                            </td>
                                            <td>
                                                {{ $item->item_code }}
                                            </td>
                                            <td>
                                                {{ $item->quantity }}
                                            </td>
                                            <td>
                                                {{ $item->stock }}
                                            </td>
                                            <td>
                                                {{ $item->price }}
                                            </td>
                                            <td>
                                                <a href='{{ route('edit-item', ['id' => $item->id]) }}' type='button'
                                                    class='btn btn-success btn-sm'>Edit</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>

                            </table>

                        </div>
                        <div class="d-flex justify-content-end">
                            {!! $items->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
    </div>

    <script src='{{ asset('/') }}public/custom/api.js'></script>
    <script src='{{ asset('/') }}public/custom/bakery.js'></script>
@endsection
