@extends('admin.master')
@section('home')
    <div class="content-wrapper">
        <style>
            .myTable {
                width: 100%;
                text-align: left;
                background-color: lemonchiffon;
                border-collapse: collapse;
            }

            .myTable th {
                background-color: red;
                color: white;
            }

            .myTable td,
            .myTable th {
                padding: 10px;
                border: 1px solid green;
            }

            .blink {
                animation: blinker 4s linear infinite;
                color: yellow;
                font-size: 20px;
                font-weight: bold;
                font-family: sans-serif;
            }

            .custom-card {
                background-color: #318777;
                color: white;
                /* #17A2B8; */
            }

            .card-body {
                background-color: lemonchiffon;
            }

            @keyframes blinker {
                50% {
                    opacity: 0;
                }
            }

        </style>
        <div class="row">
            <div class="col-xs-12">
                <h4 class="page-title"> </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header custom-card text-center"><b class="blink">Bakery stock Report</b></div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class='col-md-4'>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">Item Name Or Code</label>
                                        <input type='text' class='form-control item_info' name='item_info' id='item_info'>
                                        <div id='suggest'>

                                        </div>
                                    </div>
                                </div>

                                <div class='col-md-4'>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">From </label>
                                        <input type='date' class='form-control' name='from' id='from'>
                                    </div>
                                </div>
                                <div class='col-md-4'>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">To </label>
                                        <input type='date' class='form-control' name='to' id='to'>
                                    </div>
                                </div>
                                <div class='col-md-4'>
                                    <div class="form-group">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class='col-md-12'>
                            <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-4">
                                    <div class="form-group text-center">
                                        <label for="exampleFormControlSelect1"></label>
                                        <button type='button' onclick='itemStockreport(this)'
                                            class='btn btn-success btn-sm'>Search</button>
                                    </div>
                                </div>
                                <div class="col-md-4"></div>
                            </div>
                        </div>
                        <div class='text-right'>

                        </div>
                        <p id="dateMessage" style="color: red" class="text-center"></p>
                        <div id='bakery_itemstock_result'>

                        </div>
                        <div class="d-flex justify-content-end">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </section>
    </div>
    <script src='{{ asset('/') }}public/custom/api.js'></script>
    <script src='{{ asset('/') }}public/custom/bakery.js'></script>
@endsection
