<!-- Modal -->
<div class="modal fade" id="addWasted" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header custom-card">
        <h5 class="modal-title" id="exampleModalLongTitle">Bakery Wasted Item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body card-body">
        <label id="validation-errors"></label>
        <div class='form-group'>
          <label>Item Name</label>
          <select class="form-control searchwItem input-sm" style="width:100%" name="item_id" id="item_id">
            <option value="0">Choose One</option>
            @foreach(App\Models\BakeryItem::where('stock', '>', 0)->cursor() as $v_allItem)
            <option value="{{$v_allItem->id}}">{{$v_allItem->item_name}}</option>
            @endforeach
          </select>

        </div>
        <div class='form-group'>
          <label>Quantity</label>
          <input type="number" name="quantity" id="quantity" class="form-control input-sm"/>

        </div>
        <div class='form-group'>
          <label>Remarks</label>
          <textarea name="remarks" id="remarks" class="form-control input-sm">
          </textarea>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
        <button type="button" onclick="wastedItem(this)" class="btn btn-primary btn-loader btn-sm">Save changes</button>
      </div>
    </div>
  </div>
</div>
<script src='{{asset('/')}}public/custom/api.js'></script>
<script src='{{ asset('/') }}public/custom/bakery.js'></script>
