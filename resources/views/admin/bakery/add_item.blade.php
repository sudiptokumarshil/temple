@extends('admin.master')
@section('home')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add New Item</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form>
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Item Code</label>
                    <input type="text" required name='item_code' class="form-control" id="item_code" placeholder="Enter Item Code">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Item Name</label>
                    <input type="text" required name='item_name' class="form-control" id="item_name" placeholder="Enter Item Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Item Quantity</label>
                    <input type="number" required name='quantity' class="form-control" id="quantity" placeholder="Enter Item Quantity">
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Item Price</label>
                    <input type="number" required name='price' class="form-control" id="price" placeholder="Enter Item Price">
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1">Date</label>
                    <input type="date" required name='date' class="form-control" id="date" placeholder="Enter Item Quantity">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button onclick='addItem(this)' type="button" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script src='{{asset('/')}}public/custom/api.js'></script>
  <script src='{{asset('/')}}public/custom/bakery.js'></script>
@endsection