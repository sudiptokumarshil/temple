<table id="example"
class="display myTable datatable table-responsive w-100 d-block d-md-table table table-stripped table-bordered mt-3">
    <thead>
        <tr>
            <th>Sl</th>
            <th>Customer Name</th>
            <th>Customer Phone</th>
            <th>paid</th>
            <th>Due</th>
            <th>Quantity</th>
            <th>Price</th>
            <th>Updated By</th>
            <th>Sale Date</th>
            <th>Updated Date</th>
            <th>Action </th>
        </tr>
    </thead>
    <h4 style="color:red" class="text-center">From {{ $start }} To {{ $end }}</h4>
    <tbody>
        @php($i = 1)
        @php($allpaid = 0)
        @php($totalPaid = 0)
        @php($allDue = 0)
        @php($totalDue = 0)
        @foreach ($report as $b_report)
            <tr>
                <td>{{ $i++ }}</td>
                <td>
                    {{ $b_report->customer_name }}
                </td>
                <td>
                    {{ $b_report->phone }}
                </td>

                <td>
                    {{ $b_report->paid }}
                </td>
                <td>
                    {{ $b_report->due }}
                </td>
                <td>
                    {{ $b_report->quantity }}
                </td>
                <td>
                    {{ $b_report->price }}
                </td>
                @if ($b_report->user_name)
                    <td>
                        {{ $b_report->user_name }}
                    </td>
                @else
                    <td>
                        <p style="color:red">Not Updated Yet</p>
                    </td>
                @endif
                <td>
                    {{ $b_report->date }}
                </td>
                @if ($b_report->updated_at)
                    <td>
                        {{ $b_report->updated_at }}
                    </td>
                @endif
                <td>
                    <a href='{{ route('bakery-invoice', ['id' => $b_report->id]) }}' type='button'
                        class='btn btn-danger btn-sm'>Details</a>
                    <input type="hidden" value="{{ $totalPaid = $allpaid += $b_report->paid }}" />
                    <input type="hidden" value="{{ $totalDue = $allDue += $b_report->due }}" />
                    @if ($b_report->due > 0)
                        <button type="button" onclick='editInvoice({{ $b_report->id }})'
                            class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editInvoice">
                            Edit
                        </button>
                    @else
                    @endif
                </td>
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>
                <p style="color:red; display-inline-block">Total Paid</p>
                {{ $totalPaid }}
            </td>
            <td>
                <p style="color:red">Total Due</p> {{ $totalDue }}
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>

</table>
@include('admin.bakery.modal.edit_invoice')
<script src='{{ asset('/') }}public/custom/api.js'></script>
<script src='{{ asset('/') }}public/custom/bakery.js'></script>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [{
                    extend: 'copy',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
                    }
                },
                'colvis'
            ]
        });
    });
</script>
