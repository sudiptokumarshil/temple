<table id="example"
    class="display myTable datatable table-responsive w-100 d-block d-md-table table table-stripped table-bordered mt-3">
    <thead>
        <tr>
            <th>Sl</th>
            <th>Item Name</th>
            <th>Item Code</th>
            <th>Received Quantity</th>
            <th>Stock Available</th>
            <th>Sold</th>
            <th>Price</th>
            <th>Entry Date</th>
            <th>Action </th>
        </tr>
    </thead>
    <tbody>
        @php($i = 1)
        @foreach ($items as $item)
            <tr>
                <td>{{ $i++ }}</td>
                <td>
                    {{ $item->item_name }}
                </td>
                <td>
                    {{ $item->item_code }}
                </td>

                <td>
                    {{ $item->quantity }}
                </td>
                <td>
                    {{ $item->stock }}
                </td>
                <td>
                    {{ $item->sold }}
                </td>
                <td>
                    {{ $item->price }}
                </td>
                <td>
                    {{ date('Y-m-d', strtotime($item->created_at)) }}
                </td>
                <td>
                    <a href='{{ route('bakerytock-details', ['id' => $item->id]) }}' type='button'
                        class='btn btn-danger btn-sm'>Details</a>

                </td>
            </tr>
        @endforeach
    </tbody>
</table>

<script>
    $(document).ready(function() {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [{
                    extend: 'copy',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7]
                    }
                },
                {
                    extend: 'excel',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7]
                    }
                },
                {
                    extend: 'pdf',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7]
                    }
                },
                {
                    extend: 'csv',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7]
                    }
                },
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7]
                    }
                },
                'colvis'
            ]
        });
    });
</script>
