<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $countBakeryItem = DB::table('bakery_items')->count();
        $countBookstakkItem = DB::table('bookstall_items')->count();
        $nityasevaMember = DB::table('customers')->count();
        $totalusers = DB::table('offices')->count();
        $user = Auth::user();
        return view('admin.home.home', [
            'countBakeryItem' => $countBakeryItem,
            'countBookstakkItem' => $countBookstakkItem,
            'nityasevaMember' => $nityasevaMember,
            'totalusers' => $totalusers,
            'user' => $user
        ]);
    }
}
