<?php

namespace App\Http\Controllers\Bakery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BakeryItem;
use DB;
use App\Models\BakeryWasteditem;
class WastedItemController extends Controller
{
    public function index()
    {
        $wastedItems = DB::table('bakery_wasteds')
            ->leftjoin('bakery_items', 'bakery_wasteds.item_id', 'bakery_items.id')
            ->leftjoin('users', 'bakery_wasteds.created_by', 'users.id')
            ->select('bakery_wasteds.*', 'bakery_items.item_name', 'bakery_items.item_code', 'users.name as user_name')
            ->orderBy('id', 'desc')
            ->paginate(10);
        return view('admin.bakery.wasted_item', [
            'wastedItems' => $wastedItems,
        ]);
    }

    public function create(Request $request)
    {
        $request->validate([
            'item_id' => 'required',
            'quantity' => 'required',
            'remarks' => 'required'
        ]);
        BakeryWasteditem::wastedCreateInfo($request);
    }


}
