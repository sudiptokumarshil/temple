<?php

namespace App\Http\Controllers\Bakery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BakeryItemSell;
use Auth;
use DB;

class SaleController extends Controller
{

    public function index()
    {
        // $bakeries = BakeryItem::where('stock', '>', 0)->orderBy('id', 'desc')->get();
        return view('admin.bakery.sale', [
            // 'bakeries' => $bakeries
        ]);
    }


    public function bakery_confim_order(Request $request)
    {
        DB::beginTransaction();
        try {
            BakeryItemSell::bakery_confim_order_info($request);
            DB::commit();
            if (auth()->user()->role_id == 8) {
                return response()->json([
                    'type' => 8
                ]);
            }
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            dd($e);
            // something went wrong
        }
    }

    public function bookstall_invoice_update(Request $request, $id)
    {
        $update = BakeryItemSell::find($id);
        $update->due = $request->due;
        $update->paid = $request->paid;
        $update->updated_by = Auth::user()->id;
        $update->save();
    }

    public function bakery_invoice($id)
    {

        $invoice = BakeryItemSell::find($id);
        $item = DB::table('bakeryinvoices')
            ->join('bakery_items', 'bakeryinvoices.item_id', 'bakery_items.id')
            ->join('bakery_items_sells', 'bakeryinvoices.bakerysales_id', 'bakery_items_sells.id')
            ->select('bakeryinvoices.*', 'bakery_items.item_name')
            ->where('bakeryinvoices.bakerysales_id', $id)
            ->get();

        return view('admin.bakery.invoice', [
            'invoice' => $invoice,
            'item' => $item
        ]);
    }

    public function bakery_invoice_print($id)
    {
        $invoice = BakeryItemSell::find($id);
        $item = DB::table('bakeryinvoices')
            ->join('bakery_items', 'bakeryinvoices.item_id', 'bakery_items.id')
            ->join('bakery_items_sells', 'bakeryinvoices.bakerysales_id', 'bakery_items_sells.id')
            ->select('bakeryinvoices.*', 'bakery_items.item_name')
            ->where('bakeryinvoices.bakerysales_id', $id)
            ->get();

        return view('admin.bakery.invoice_print', [
            'invoice' => $invoice,
            'item' => $item
        ]);
    }
}
