<?php

namespace App\Http\Controllers\Bakery;

use App\Http\Controllers\Controller;
use App\Models\BakeryItem;
use App\Models\BakeryItemSell;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\BakeryStockReport;
use DB;
use Cart;

class BakeryController extends Controller
{

    public function index()
    {
        return view('admin.bakery.add_item');
    }

    public function save_item(Request $request)
    {
        $request->validate([
            'code' => 'required',
            'name' => 'required',
            'qty' => 'required',
            'price' => 'required',
        ]);
        BakeryItem::save_item_info($request);
    }

    public function all_item()
    {
        $items = BakeryItem::orderBy('id', 'desc')->paginate(10);
        return view('admin.bakery.all_item', compact('items'));
    }

    public function edit_item($id)
    {
        $item = BakeryItem::find($id);
        return view('admin.bakery.edit_item', compact('item'));
    }

    public function update_item(Request $request, $id)
    {
        BakeryItem::update_item_info($request, $id);
        return response()->json([
            'message' => 'Item Updated Successfully!!'
        ]);
    }


    public function report()
    {
        $reports = DB::table('bakery_items_sells')
            ->leftjoin('users', 'bakery_items_sells.updated_by', 'users.id')
            ->select('bakery_items_sells.*', 'users.name as user_name')
            ->orderBy('id', 'desc')->paginate(10);

        return view('admin.bakery.report', compact('reports'));
    }

    public function edit_item_inv($id)
    {
        $edit = BakeryItemSell::find($id);
        return $edit;
    }

    public function bakery_result(Request $request)
    {
        $item_info = $request->item_info;
        $items = DB::table("bakery_items")
            ->whereRaw(" (`id` like ? or `item_name` like ? or `item_code` like ? ) ", ["%" . $item_info . "%", "%" . $item_info . "%", "%" . $item_info . "%"])->get();

        return view('admin.bakery.bakery_result', compact('items'));
    }


    public function report_result(Request $request)
    {
        $customer_name = $request->customer_name;
        $start = date($request->start_date);
        $end = date($request->end_date);

        $report = BakeryItemSell::query()
            ->leftjoin('users', 'bakery_items_sells.updated_by', 'users.id')
            ->select('bakery_items_sells.*', 'users.name as user_name')
            ->where(function ($filter) use ($customer_name, $start, $end) {
                if (!empty($customer_name)) {
                    $filter->where('customer_name', '=', $customer_name);
                }
            })->whereBetween('date', [$start, $end])->orderBy('id', 'desc')->get();
        return view('admin.bakery.report_result', compact('report', 'start', 'end'));
    }


    // show shopping bag bakery item .. .. .. .. .. .. ..

    public function bakery_sales_form()
    {
        $cart = Cart::instance('bakery')->content();
        $total = Cart::instance('bakery')->priceTotal();

        // return $bakerycart;
        return view('admin.bakery.sales_form', [
            'cart' => $cart,
            'total' => $total,
        ]);
    }

    public function customer_suggest(Request $request)
    {
        $data = array();
        $item_info = $request->query;
        if ($request->get('query')) {
            $query = $request->get('query');
            $result = DB::table('bakery_items_sells')
                ->select('id','customer_name')
                ->where('customer_name', 'LIKE', "%{$query}%")
                ->get();

            foreach ($result as $val) {
                $ara = array();
                $ara["id"] = $val->id;
                $ara["value"] = $val->customer_name;
                array_push($data, $ara);
            }
        }
        echo json_encode($data);
    }


    public function bakeyr_stock_report()
    {
        return view('admin.bakery.bakery_stock_report');
    }

    public function bakeyr_stock_report_result(Request $request)
    {
        $item_info = $request->item_info;
        $from = date($request->from);
        $to = date($request->to);
        $items = BakeryItem::query()
            ->where(function ($filter) use ($item_info, $from, $to) {
                if (!empty($item_info)) {
                    $filter->where("item_name", 'like', $item_info)->orwhere('item_code', 'like', $item_info);
                }
            })->whereBetween('created_at', [$from, $to])->orderBy('id', 'desc')->get();

        return view('admin.bakery.bakery_stock_report_result', compact('items'));
    }


    public function stock_report_details($id)
    {
        $item = BakeryItem::find($id);
        $reports = DB::table('bakery_stock_report')
            ->where('item_id', $id)
            ->get();
        return view('admin.bakery.stock_report_details', [
            'item' => $item,
            'reports' => $reports
        ]);
    }
}
