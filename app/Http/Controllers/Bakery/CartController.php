<?php

namespace App\Http\Controllers\Bakery;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cart;
use Session;
use App\Models\BakeryItem;
use DB;

class CartController extends Controller
{
    public function bakery_add_to_cart(Request $request)
    {
        // return $request->all();
        $books = BakeryItem::find($request->id);
        Cart::instance('bakery')->add([
            'id' => $request->id,
            'name' => $books->item_name,
            'qty' => $request->qty,
            'price' => $books->price,
            'weight' => 550,
            'options' =>
            [
                'discount' => 0,
                'percent' => 0,
            ],
        ]);

        $count = Cart::instance('bakery')->count();
        $data = array(
            "count" => $count,
        );
        echo json_encode($data);
    }

    public function isCartExist()
    {
        $total = Cart::subtotal();
        $data = array(
            "total" => $total,
        );
        echo json_encode($data);
    }

    public function delete_cart($id)
    {
        Cart::instance('bakery')->remove($id);
        $this->getCartInfoAfterChange();
    }

    public function update_cart(Request $request)
    {
        $message = '';
        $status  = 0;
        $itemId = intval($request->item_id);
        $checkStockQty = DB::table('bakery_items')
            ->where('id', $itemId)
            ->first();
        if ($checkStockQty->stock >= $request->qty) {
            Cart::instance('bakery')->update($request->rowId, $request->qty);
            return  $this->getCartInfoAfterChange();
        } else {
            $message = 'Stock Out!!';
            $status = 1;
            return [
                'message' => $message,
                'status' => $status,
            ];
        }
    }

    public function getCartInfoAfterChange()
    {
        ///$info=Cart::get($request->rowId);
        $info = array();
        $sub_total = Cart::instance('bakery')->subtotal();
        $total = intval(Cart::instance('bakery')->total());
        $cart_list = Cart::instance('bakery')->content();
        $count = Cart::instance('bakery')->content()->count();
        $data = array(
            "row" => $info,
            "list" => $cart_list,
            "sub_total" => $sub_total,
            "total" => $total,
            "count" => $count,
        );
        echo json_encode($data);
    }
}
