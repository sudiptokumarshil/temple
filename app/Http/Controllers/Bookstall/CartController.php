<?php

namespace App\Http\Controllers\Bookstall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cart;
use App\Models\BookstallItem;
use DB;

class CartController extends Controller
{
    public function add_to_cart(Request $request)
    {
        $books = BookstallItem::find($request->id);
        Cart::add([
            'id' => $request->id,
            'name' => $books->item_name,
            'qty' => $request->qty,
            'price' => $books->price,
            'weight' => 550,
            'options' =>
            [

                'discount' => 0,
                'percent' => 0,
            ],
        ]);

        $count = Cart::content()->count();
        $data = array(
            "count" => $count,
        );
        echo json_encode($data);
    }

    public function isCartExist()
    {
        $total = Cart::subtotal();
        $data = array(
            "total" => $total,
        );
        echo json_encode($data);
    }

    public function delete_cart($id)
    {
        Cart::remove($id);
        $this->getCartInfoAfterChange();
    }

    public function update_cart(Request $request)
    {
        $message = '';
        $status  = 0;
        $itemId = intval($request->item_id);
        $checkStockQty = DB::table('bookstall_items')
            ->where('id', $itemId)
            ->first();
        if ($checkStockQty->stock >= $request->qty) {
            Cart::update($request->rowId, $request->qty);
            return $this->getCartInfoAfterChange();
        } else {
            $message = 'Stock Out!!';
            $status = 1;
            return [
                'message' => $message,
                'status' => $status,
            ];
        }
    }

    public function getCartInfoAfterChange()
    {
        $info = array();
        $sub_total = Cart::subtotal();
        $total = intval(Cart::total());
        $cart_list = Cart::content();
        $count = Cart::content()->count();
        $data = array(
            "row" => $info,
            "list" => $cart_list,
            "sub_total" => $sub_total,
            "total" => $total,
            "count" => $count,
        );
        echo json_encode($data);
    }
}
