<?php

namespace App\Http\Controllers\Bookstall;

use App\Http\Controllers\Controller;
use App\Models\BookstallItem;
use App\Models\BookItemsell;
use Illuminate\Http\Request;
use Cart;
use DB;
use Auth;

class BookstallController extends Controller
{
    public function index()
    {
        return view('admin.bookstall.add-book');
    }

    public function save_book(Request $request)
    {
        $request->validate([
            'item_code' => 'required|unique:bookstall_items|max:255',
            'item_name' => 'required|unique:bookstall_items|max:255',
            'quantity' => 'required',
            'price' => 'required',
        ]);
        BookstallItem::save_book_info($request);
        return response()->json([
            'message' => 'Book Added Successfully!!'
        ]);
    }

    public function all_book()
    {
        $book = BookstallItem::orderBy('id', 'desc')->paginate(10);
        return view('admin.bookstall.all-book', compact('book'));
    }

    public function edit_book($id)
    {
        $book = BookstallItem::find($id);
        return view('admin.bookstall.edit-book', compact('book'));
    }

    public function update_book(Request $request, $id)
    {
        BookstallItem::update_book_info($request, $id);
        return response()->json([
            'message' => 'Book Updated Successfully!!'
        ]);
    }

    public function report()
    {
        $reports = DB::table('book_itemsells')
            ->leftjoin('users', 'book_itemsells.updated_by', 'users.id')
            ->select('book_itemsells.*', 'users.name as user_name')
            ->orderBy('id', 'desc')->paginate(50);
        return view('admin.bookstall.report', compact('reports'));
    }

    public function bookstall_invoice($id)
    {

        $invoice = BookItemsell::find($id);
        $item = DB::table('book_invoices')
            ->join('bookstall_items', 'book_invoices.item_id', 'bookstall_items.id')
            ->join('book_itemsells', 'book_invoices.bookSales_id', 'book_itemsells.id')
            ->select('book_invoices.*', 'bookstall_items.item_name')
            ->where('book_invoices.bookSales_id', $id)
            ->get();

        return view('admin.bookstall.invoice', [
            'invoice' => $invoice,
            'item' => $item
        ]);
    }

    public function bookstall_invoice_print($id)
    {
        $invoice = BookItemsell::find($id);
        $item = DB::table('book_invoices')
            ->join('bookstall_items', 'book_invoices.item_id', 'bookstall_items.id')
            ->join('book_itemsells', 'book_invoices.bookSales_id', 'book_itemsells.id')
            ->select('book_invoices.*', 'bookstall_items.item_name')
            ->where('book_invoices.bookSales_id', $id)
            ->get();

        return view('admin.bookstall.invoice_print', [
            'invoice' => $invoice,
            'item' => $item
        ]);
    }

    public function book_result(Request $request)
    {
        $item_info = $request->item_info;
        $items = DB::table("bookstall_items")
            ->whereRaw(" (`id` like ? or `item_name` like ? or `item_code` like ? ) ", ["%" . $item_info . "%", "%" . $item_info . "%", "%" . $item_info . "%"])->get();

        return view('admin.bookstall.book_result', compact('items'));
    }

    public function report_result(Request $request)
    {
        $customer_name = $request->customer_name;
        $start = date($request->start_date);
        $end = date($request->end_date);

        $report = BookItemsell::query()
            ->leftjoin('users', 'book_itemsells.updated_by', 'users.id')
            ->select('book_itemsells.*', 'users.name as user_name')
            ->where(function ($filter) use ($customer_name) {
                if (!empty($customer_name)) {
                    $filter->where('customer_name', '=', $customer_name);
                }
            })->whereBetween('date', [$start, $end])->orderBy('id', 'desc')->get();
        return view('admin.bookstall.report_result', compact('report', 'start', 'end'));
    }

    public function bookstall_sale()
    {
        return view('admin.bookstall.sale');
    }

    public function delete_item($id)
    {
        $book = BookstallItem::find($id);
        $book->delete();
        return redirect()->back()->with('message', 'Item Deleted Successfully!!');
    }

    public function book_filter(Request $request)
    {
        $item_info = $request->item_info;
        $books = DB::table("bookstall_items")
            ->whereRaw(" (`id` like ? or `item_name` like ? or `item_code` like ? ) ", ["%" . $item_info . "%", "%" . $item_info . "%", "%" . $item_info . "%"])->get();

        return view('admin.bookstall.book_filter', compact('books'));
    }


    public function book_sales_form()
    {
        $cart = Cart::content();
        $total = Cart::priceTotal();
        return view('admin.bookstall.sales_form', [
            'cart' => $cart,
            'total' => $total,
        ]);
    }

    public function bookstall_confim_order(Request $request)
    {
        BookItemsell::bookstall_confim_order($request);
        if (auth()->user()->role_id == 9) {
            return response()->json([
                'type' => 9
            ]);
        }
    }

    public function bookstall_invoice_edit($id)
    {
        $edit = BookItemsell::find($id);
        return $edit;
    }

    public function bookstall_invoice_update(Request $request, $id)
    {
        $update = BookItemsell::find($id);
        $update->due = $request->due;
        $update->paid = $request->paid;
        $update->updated_by = Auth::user()->id;
        $update->save();
    }

    public function itemstock_report()
    {
        return view('admin.bookstall.itemstock_report');
    }

    public function itemstock_report_result(Request $request)
    {
        $item_info = $request->item_info;
        $from = date($request->from);
        $to = date($request->to);
        $items = BookstallItem::query()
            ->where(function ($filter) use ($item_info, $from, $to) {
                if (!empty($item_info)) {
                    $filter->where("item_name", 'like', $item_info)->orwhere('item_code', 'like', $item_info);
                }
            })->whereBetween('created_at', [$from, $to])->orderBy('id', 'desc')->get();

        return view('admin.bookstall.itemstock_result', compact('items'));
    }

    public function stock_report_details($id)
    {
        $item = BookstallItem::find($id);
        $reports = DB::table('bookstall_stock_report')
            ->where('item_id', $id)
            ->get();
        return view('admin.bookstall.stock_report_details', [
            'item' => $item,
            'reports' => $reports
        ]);
    }

    public function item_match_result(Request $request)
    {
        $data = array();
        $item_info = $request->query;
        if ($request->get('query')) {
            $query = $request->get('query');
            $result = DB::table('bookstall_items')
                ->where('item_name', 'LIKE', "%{$query}%")
                ->get();

            foreach ($result as $val) {
                $ara = array();
                $ara["id"] = $val->id;
                $ara["value"] = $val->item_name;
                array_push($data, $ara);
            }
        }
        echo json_encode($data);
    }

    public function customer_suggest(Request $request)
    {
        $data = array();
        $item_info = $request->query;
        if ($request->get('query')) {
            $query = $request->get('query');
            $result = DB::table('book_itemsells')
                ->select('id','customer_name')
                ->where('customer_name', 'LIKE', "%{$query}%")
                ->get();

            foreach ($result as $val) {
                $ara = array();
                $ara["id"] = $val->id;
                $ara["value"] = $val->customer_name;
                array_push($data, $ara);
            }
        }
        echo json_encode($data);
    }
}
