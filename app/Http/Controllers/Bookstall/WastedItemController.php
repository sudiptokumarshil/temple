<?php

namespace App\Http\Controllers\Bookstall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BookstallWasted;
use App\Models\BookstallItem;
use DB;

class WastedItemController extends Controller
{
    public function index()
    {
        $allItem = BookstallItem::where('stock', '>', 0)->get();
        $wastedItems = DB::table('bookstall_wasteds')
            ->leftjoin('bookstall_items', 'bookstall_wasteds.item_id', 'bookstall_items.id')
            ->leftjoin('users', 'bookstall_wasteds.created_by', 'users.id')
            ->select('bookstall_wasteds.*', 'bookstall_items.item_name', 'bookstall_items.item_code', 'users.name as user_name')
            ->orderBy('id', 'desc')
            ->paginate(5);
        return view('admin.bookstall.wasted_item', [
            'wastedItems' => $wastedItems,
            'allItem' => $allItem
        ]);
    }

    public function create(Request $request)
    {
        $request->validate([
            'item_id' => 'required',
            'quantity' => 'required',
            'remarks' => 'required'
        ]);
        BookstallWasted::wastedCreateInfo($request);
    }
}
