<?php

namespace App\Http\Controllers\Office;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Office;

class OfficeController extends Controller
{
    public function index()
    {
        $alloffice = Office::orderBy('id', 'desc')->get();
        return view('admin.offices.manage_contact', [
            'alloffice' => $alloffice
        ]);
    }


    public function add_contact()
    {
        return view('admin.offices.form.add_contact');
    }


    public function save_contact(Request $request)
    {
        $message = '';
        $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required',
            'goatra' => 'required',
            'address' => 'required',
            'qualification' => 'required',
            'occupation' => 'required',
            'collector' => 'required',
            'department' => 'required',
            'date_of_birth' => 'required',
            'phone' => 'required',
            'festival_name' => 'required',
            'pranami' => 'required',
            'age' => 'required',
        ]);
        Office::save_info($request);

        $message = "Contact Saved  Successfully!!";
        return redirect()->route('manage-contact')->with([
            'message' => $message
        ]);
    }


    public function edit($id)
    {
        $edit = Office::find($id);
        return view('admin.offices.form.edit_contact', [
            'edit' => $edit
        ]);
    }


    public function update_contact(Request $request)
    {
        $message = '';
        $request->validate([
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required',
            'goatra' => 'required',
            'address' => 'required',
            'qualification' => 'required',
            'occupation' => 'required',
            'collector' => 'required',
            'department' => 'required',
            'date_of_birth' => 'required',
            'phone' => 'required',
            'festival_name' => 'required',
            'pranami' => 'required',
            'age' => 'required',
        ]);

        Office::update_info($request);

        $message = "Contact Updated  Successfully!!";
        return redirect()->route('manage-contact')->with([
            'message' => $message
        ]);
    }
}
