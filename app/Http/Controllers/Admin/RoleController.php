<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class RoleController extends Controller
{

    public function index()
    {
        $allrole = User::all();
        return view('admin.role_management.role.manage_role', [
            'allrole' => $allrole
        ]);
    }
    public function add_role()
    {
        return view('admin.role_management.role.add_role');
    }

    public function save_role(Request $request)
    {
        $role = new User();
        $role->name = $request->name;
        $role->email = $request->email;
        $role->password = Hash::make($request->password);
        $role->role_id = $request->role_id;
        $role->save();
        return redirect("manage-role")->with("message", "Role Saved Successfully!!");
    }
    public function update_role(Request $request)
    {
        $role = User::find($request->id);
        $role->name = $request->name;
        $role->email = $request->email;
        $role->role_id = $request->role_id;
        $role->update();
        return redirect("manage-role")->with("message", "Role Updated Successfully!!");
    }

    public function change_password($id)
    {
        $edit = User::find($id);
        return view('admin.role_management.role.change_password', [
            'edit' => $edit
        ]);
    }
    public function update_password(Request $request)
    {
        $message = '';
        if ($request->password == $request->confim_password) {
            $user = User::find($request->id);
            $user->password  = Hash::make($request->confim_password);
            $user->update();
            $message = "Password Updated Successfully!!";
        } else {
            $message = "Confirm Password Does not Match!!";
        }

        return redirect("manage-role")->with("message", $message);
    }



    public function edit($id)
    {
        $edit = User::find($id);
        return view('admin.role_management.role.edit_role', [
            'edit' => $edit
        ]);
    }
}
