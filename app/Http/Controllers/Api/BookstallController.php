<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BookstallItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use DB;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Models\BookItemsell;
use Illuminate\Support\Facades\Response;

class BookstallController extends Controller
{
    public function addnewitem(Request $request)
    {
        $newitem = new BookstallItem();
        $newitem->item_code = $request->item_code;
        $newitem->item_name = $request->item_name;
        $newitem->quantity = $request->quantity;
        $newitem->price = $request->price;
        $newitem->save();
        return Response::json(array(
            'success' => true,
            'message' => 'Item saved Successfully!!.'

        ));
    }

    public function getbook_item()
    {
        $book = BookstallItem::all();
        return response()->json($book);
    }

    public function editbook_item($id)
    {
        $book = BookstallItem::find($id);
        return response()->json($book);
    }

    public function updatebook_item(Request $request)
    {
        $book = BookstallItem::find($request->id);
        $book->item_code = $request->item_code;
        $book->item_name = $request->item_name;
        $book->quantity = $request->quantity;
        $book->price = $request->price;
        $book->save();

        return Response::json(array(
            'success' => true,
            'message' => 'Book item updated successfully!!.'
        ));

    }

    public function book_item_sales_form(Request $request)
    {
        $bookitem = new BookItemsell();
        $bookitem->item_name = $request->item_name;
        $bookitem->quantity = $request->quantity;
        $bookitem->price = $request->price;
        $bookitem->customer_name = $request->customer_name;
        $bookitem->mobile_number = $request->mobile_number;
        $bookitem->paid = $request->paid;
        $bookitem->due = $request->due;
        $bookitem->date = $request->date;
        $bookitem->save();
        return Response::json(array(
            'success' => true,
            'message' => 'Bookitem form ready!!.'
        ));


    }

    public function getbook_item_sells()
    {
        $sells_book = BookItemsell::all();
        return response()->json($sells_book);
    }

    public function book_sales_item_details($id)
    {
        // return $id;
        $sells_book = BookItemsell::find($id);
        return response()->json($sells_book);
    }
}
