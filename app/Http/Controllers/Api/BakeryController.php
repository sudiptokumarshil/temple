<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BookstallItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use DB;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Models\BakeryItem;
use App\Models\BakeryItemSell;
use Illuminate\Support\Facades\Response;

class BakeryController extends Controller
{
    public function index()
    {
        $bakeryItem = BakeryItem::all();
        return response()->json([
            'status' => 200,
            'bakeryItem' => $bakeryItem
        ]);
    }

    public function add_item(Request $request)
    {
        $bakeryItem = new BakeryItem();
        $bakeryItem->item_name = $request->item_name;
        $bakeryItem->quantity = $request->quantity;
        $bakeryItem->price = $request->price;
        $bakeryItem->save();
        return response()->json([
            'status' => 200,
            'message' => 'Item Saved Successfully!!'
        ]);
    }


    public function edit_item($id)
    {
        $bakeryItem = BakeryItem::find($id);
        return response()->json([
            'status' => 200,
            'bakeryItem' => $bakeryItem
        ]);

    }

    public function update_item(Request $request, $id)
    {
        $bakeryItem = BakeryItem::find($id);
        $bakeryItem->item_name = $request->item_name;
        $bakeryItem->quantity = $request->quantity;
        $bakeryItem->price = $request->price;
        $bakeryItem->save();
        return response()->json([
            'status' => 200,
            'message' => 'Item updated Successfully!!'
        ]);
    }


    public function add_sales_item(Request $request)
    {
        $sellsitem = new BakeryItemSell();
        $sellsitem->item_name = $request->item_name;
        $sellsitem->quantity = $request->quantity;
        $sellsitem->price = $request->price;
        $sellsitem->customer_name = $request->customer_name;
        $sellsitem->phone = $request->phone;
        $sellsitem->paid = $request->paid;
        $sellsitem->due = $request->due;
        $sellsitem->date = $request->date;
        $sellsitem->save();

        return response()->json([
            'status' => 200,
            'message' => 'Sells Item form saved Successfully!!'
        ]);
    }


    public function show_all_sells_item()
    {
        $sellsitem = BakeryItemSell::all();
        return response()->json([
            'status' => 200,
            'sellsitem' => $sellsitem
        ]);
    }


    public function details_bakery_item($id)
    {
        $sellsitem = BakeryItemSell::find($id);
        return response()->json([
            'status' => 200,
            'sellsitem' => $sellsitem
        ]);
    }

    public function edit_sell_item($id)
    {
        $sellsitem = BakeryItemSell::find($id);
        return response()->json([
            'status' => 200,
            'sellsitem' => $sellsitem
        ]);
    }

    public function update_sells_item(Request $request,$id)
    {
        $sellsitem = BakeryItemSell::find($id);
        $sellsitem->item_name = $request->item_name;
        $sellsitem->quantity = $request->quantity;
        $sellsitem->price = $request->price;
        $sellsitem->customer_name = $request->customer_name;
        $sellsitem->phone = $request->phone;
        $sellsitem->paid = $request->paid;
        $sellsitem->due = $request->due;
        $sellsitem->date = $request->date;
        $sellsitem->save();

        return response()->json([
            'status' => 200,
            'message' => 'Sells Item form Updated Successfully!!'
        ]);
    }
}