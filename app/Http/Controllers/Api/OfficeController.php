<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\BookstallItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use DB;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Models\BookItemsell;
use Illuminate\Support\Facades\Response;
use App\Models\Office;
class OfficeController extends Controller
{
    public function add_contact_view_form(Request $request)
    {
        $office = new Office();
        $office->name = $request->name;
        $office->goatra = $request->goatra;
        $office->phone = $request->phone;
        $office->festival_name = $request->festival_name;
        $office->date = $request->date;
        $office->pranami = $request->pranami;
        $office->department = $request->department;
        $office->collector = $request->collector;
        $office->save();

        return Response::json(array(
            'success' => true,
            'message' => 'Contact  form Saved Successfully!!.'
        ));
    }
    
}