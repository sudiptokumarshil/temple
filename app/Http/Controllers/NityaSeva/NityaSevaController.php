<?php

namespace App\Http\Controllers\NityaSeva;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\NityaSeva\Customer;
use App\Models\NityaSeva\Pranami;
use DB;
use Intervention\Image\Facades\Image;

class NityaSevaController extends Controller
{
    public function index()
    {
        return view('admin.nityaseva.add-customer');
    }


    public function all_customer()
    {
        $customers = Customer::orderby('id', 'desc')->paginate(10);
        return view('admin.nityaseva.all-customer', compact('customers'));
    }

    public function edit_customer($id)
    {
        $edit =  Customer::find($id);
        return view('admin.nityaseva.edit-customer', [
            'edit' => $edit
        ]);
    }


    public function save_customer(Request $request)
    {
        $message = '';
        $request->validate([
            'customer_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required',
            'gotra' => 'required',
            'address' => 'required',
            'qualification' => 'required',
            'occupation' => 'required',
            'counsellor' => 'required',
            'blood_group' => 'required',
            'department' => 'required',
            'date_of_birth' => 'required',
            'phone' => 'required',
            'member_id' => 'required',
            'pranami' => 'required',
        ]);
        $rand = rand(5, 10);
        if(!empty($request->hasFile('customer_image'))){
        $customerName = $request->name;
        $customer = new Customer;
        $customer->name = $customerName;
        $customer->gotra = $request->gotra;
        $customer->address = $request->address;
        $customer->member_id = $request->member_id;
        $customer->pranami = $request->pranami;
        $customer->qualification = $request->qualification;
        $customer->occupation = $request->occupation;
        $customer->counsellor = $request->counsellor;
        $customer->blood_group = $request->blood_group;
        $customer->department = $request->department;
        $customer->date_of_birth = $request->date_of_birth;
        $customer->phone = $request->phone;
        $customer->age = $request->age;
        if ($request->hasFile('customer_image')) {
            if ($customer->customer_image) {
                unlink($customer->customer_image);
            }
            $image = $request->file('customer_image');
            $imageName = $image->getClientOriginalName();
            $fileName = trim($customerName) . "_" . "_" . $rand . $imageName;
            $directory = 'public/customer_image/';
            $imageUrl = $directory . $fileName;
            Image::make($image)->resize(320, 240)->save($imageUrl);
            $customer->customer_image = $imageUrl;
        }
        $customer->save();
      } else{
        $customerName = $request->name;
        $customer = new Customer;
        $customer->name = $customerName;
        $customer->gotra = $request->gotra;
        $customer->address = $request->address;
        $customer->member_id = $request->member_id;
        $customer->pranami = $request->pranami;
        $customer->qualification = $request->qualification;
        $customer->occupation = $request->occupation;
        $customer->counsellor = $request->counsellor;
        $customer->blood_group = $request->blood_group;
        $customer->department = $request->department;
        $customer->date_of_birth = $request->date_of_birth;
        $customer->age = $request->age;
        $customer->phone = $request->phone;
        $customer->save();
      }


        $message = "Customer Saved  Successfully!!";
        return redirect()->route('all-customer')->with([
            'message' => $message
        ]);
    }

    public function update_customer(Request $request)
    {
        $message = '';

        $request->validate([
            'name' => 'required',
            'gotra' => 'required',
            'address' => 'required',
            'qualification' => 'required',
            'occupation' => 'required',
            'counsellor' => 'required',
            'blood_group' => 'required',
            'department' => 'required',
            'date_of_birth' => 'required',
            'phone' => 'required',
            'member_id' => 'required',
            'pranami' => 'required',
        ]);
        if (!empty($request->hasFile('customer_image'))) {
            $rand = rand(5, 10);
            $customerName = $request->name;
            $customer = Customer::find($request->id);
            $customer->name = $customerName;
            $customer->gotra = $request->gotra;
            $customer->address = $request->address;
            $customer->qualification = $request->qualification;
            $customer->occupation = $request->occupation;
            $customer->counsellor = $request->counsellor;
            $customer->blood_group = $request->blood_group;
            $customer->department = $request->department;
            $customer->member_id = $request->member_id;
            $customer->pranami = $request->pranami;
            $customer->date_of_birth = $request->date_of_birth;
            $customer->phone = $request->phone;
            $customer->age = $request->age;
            if ($request->hasFile('customer_image')) {
                if ($customer->customer_image) {
                    unlink($customer->customer_image);
                }
                $image = $request->file('customer_image');
                $imageName = $image->getClientOriginalName();
                $fileName = trim($customerName) . "_" . "_" . $rand . $imageName;
                $directory = 'public/customer_image/';
                $imageUrl = $directory . $fileName;
                Image::make($image)->resize(320, 240)->save($imageUrl);
                $customer->customer_image = $imageUrl;
            }
            $customer->update();
        } else {
            $customerName = $request->name;
            $customer = Customer::find($request->id);
            $customer->name = $customerName;
            $customer->gotra = $request->gotra;
            $customer->address = $request->address;
            $customer->qualification = $request->qualification;
            $customer->occupation = $request->occupation;
            $customer->counsellor = $request->counsellor;
            $customer->blood_group = $request->blood_group;
            $customer->department = $request->department;
            $customer->date_of_birth = $request->date_of_birth;
            $customer->phone = $request->phone;
            $customer->member_id = $request->member_id;
            $customer->pranami = $request->pranami;
            $customer->age = $request->age;
            $customer->update();
        }

        $message = "Customer Updated  Successfully!!";

        return redirect()->route('all-customer')->with([
            'message' => $message
        ]);
    }

    public function add_pranami()
    {
        $customers = Customer::all();
        return view('admin.nityaseva.pranami-form', compact('customers'));
    }

    // save and update in one method
    public function save_pranami(Request $request)
    {
        if (!empty($request->id)) {
            Pranami::find($request->id)->update($request->all());
            $message = 'Pranami Updated Successfully!!';
        } else {
            Pranami::create($request->all());
            $message = 'Pranami Saved Successfully!!';
        }

        return response()->json([
            'message' => $message
        ]);
    }

    public function report()
    {
        $customers = Customer::all();
        $pranami = DB::table('pranamies')
            ->join('customers', 'pranamies.customer_id', 'customers.id')
            ->select('pranamies.*', 'customers.name as customer_name', 'customers.phone as customer_phone')
            ->orderby('pranamies.id', 'desc')
            ->paginate(10);
        return view('admin.nityaseva.report', compact('pranami', 'customers'));
    }

    public function report_result(Request $request)
    {
        $customer_id = (int) $request->customer_id;
        $start = date($request->start_date);
        $end = date($request->end_date);

        $report = Pranami::query()
            ->join('customers', 'pranamies.customer_id', 'customers.id')
            ->select('pranamies.*', 'customers.name as customer_name', 'customers.phone as customer_phone')
            ->where(function ($filter) use ($customer_id, $start, $end) {
                if (!empty($customer_id)) {
                    $filter->where('customer_id', '=', $customer_id);
                }
            })->whereBetween('pranamies.date', [$start, $end])->orderBy('pranamies.id', 'desc')->get();
        return view('admin.nityaseva.report_result', compact('report'));
    }


    public function edit_pranami($id)
    {
        return Pranami::find($id);
    }
}
