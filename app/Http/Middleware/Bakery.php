<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
class Bakery
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check() && Auth::user()->role_id == 2 || Auth::check() && Auth::user()->role_id == 3 || Auth::check() && Auth::user()->role_id == 6) {
            return $next($request);
        }
        abort(403);
    }
}
