<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;
class Office
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::check() && Auth::user()->role_id == 5 || Auth::check() && Auth::user()->role_id == 3 || Auth::check() && Auth::user()->role_id == 7) {
            return $next($request);
        }
        abort(403);
    }
}
