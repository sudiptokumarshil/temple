<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OfficeMember extends Model
{
    protected $guarded = [];
}
