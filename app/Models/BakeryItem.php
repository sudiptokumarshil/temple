<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BakeryItemSell;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\BakeryStockReport;
use DB;

class BakeryItem extends Model
{
    protected $guarded = [];
    protected $table = 'bakery_items';

    public static function save_item_info($request)
    {
        DB::beginTransaction();
        try {

            $additem = new BakeryItem();
            $additem->item_code = $request->code;
            $additem->item_name = $request->name;
            $additem->quantity  = $request->qty;
            $additem->price = $request->price;
            $additem->date = $request->date;
            $additem->stock = $request->qty;
            $additem->save();

            $report = new BakeryStockReport();
            $report->item_id = $additem->id;
            $report->quantity = $additem->quantity;
            $report->price = $additem->price;
            $report->stock = $additem->stock;
            $report->save();
            DB::commit();
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
        }
    }

    public static function update_item_info($request, $id)
    {
        DB::beginTransaction();
        try {
            $item = BakeryItem::find($id);
            $item->item_code = $request->code;
            $item->item_name = $request->name;
            $item->price = $request->price;
            $item->date = $request->date;
            $item->quantity =  $item->quantity += $request->qty;
            $item->stock =  $item->stock += $request->qty;
            $item->save();

            $report = new BakeryStockReport();
            $report->item_id = $item->id;
            $report->quantity = $request->qty;
            $report->price = $item->price;
            $report->stock = $request->qty;
            $report->save();
            DB::commit();
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
        }
    }
}
