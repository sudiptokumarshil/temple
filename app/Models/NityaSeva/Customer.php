<?php

namespace App\Models\NityaSeva;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table ='customers';
    protected $guarded =[];
}
