<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookstallStockReport extends Model
{
    protected $table = 'bookstall_stock_report';
    protected $guarded =[];
    
    public function items(){
    	 return $this->belongsTo('App\Models\BookstallItem','item_id');
    }
}
