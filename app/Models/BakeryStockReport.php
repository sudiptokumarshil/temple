<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BakeryStockReport extends Model
{
    protected $table = 'bakery_stock_report';
    protected $guarded = [];
}
