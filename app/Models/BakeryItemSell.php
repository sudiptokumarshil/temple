<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;
use Cart;
use DB;
use Auth;
use App\Models\BakeryItem;
use App\Models\BakeryInvoice;

class BakeryItemSell extends Model
{
    protected $guarded = [];
    protected $table = 'bakery_items_sells';



    public static function bakery_confim_order_info($request)
    {
        $bakeryitem = new BakeryItemSell();
            $bakeryitem->customer_name = $request->customer_name;
            $bakeryitem->phone = $request->mobile_number;
            $bakeryitem->paid = $request->paid;
            $bakeryitem->due = $request->due;
            $bakeryitem->date = $request->date;
            $bakeryitem->quantity = Cart::instance('bakery')->count();
            $bakeryitem->price = $request->price;
            $bakeryitem->address = $request->address;
            $bakeryitem->discount = intval($request->discount);
            $bakeryitem->created_by = Auth::user()->id;
            $bakeryitem->save();

            $cartProduct = Cart::instance('bakery')->content();
            foreach ($cartProduct as $v_cart) {
                $pinfo = BakeryItem::find($v_cart->id);
                $invoice = new BakeryInvoice();
                $invoice->bakerysales_id = $bakeryitem->id;
                $invoice->item_id = $v_cart->id;
                $invoice->unit_price = $v_cart->price;
                $invoice->qty = $v_cart->qty;
                $invoice->save();
                BakeryItem::where('id', $v_cart->id)->decrement('stock', abs($invoice->qty));
                BakeryItem::where('id', $v_cart->id)->increment('sold', abs($invoice->qty));
            }
            Cart::instance('bakery')->destroy();
    }
}
