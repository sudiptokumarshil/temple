<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BookItemsell;
use App\Models\Book_invoice;
use App\Models\BookstallStockReport;
use Illuminate\Http\Request;
use Session;
use Cart;
use DB;
use Auth;

class BookstallItem extends Model
{
    protected $guarded = [];

    public function items_report()
    {
        return $this->hasOne('App\Models\BookstallStockReport', 'id');
    }

    public static function save_book_info($request)
    {
        DB::beginTransaction();
        try {
            $newitem = new BookstallItem();
            $newitem->item_code = $request->item_code;
            $newitem->item_name = $request->item_name;
            $newitem->quantity = $request->quantity;
            $newitem->stock = $request->quantity;
            $newitem->price = $request->price;
            $newitem->save();

            $report = new BookstallStockReport();
            $report->item_id = $newitem->id;
            $report->quantity = $newitem->quantity;
            $report->price = $newitem->price;
            $report->stock = $newitem->stock;
            $report->save();
            DB::commit();
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
        }
    }

    public static function update_book_info($request, $id)
    {
        DB::beginTransaction();
        try {
            $book = BookstallItem::find($id);
            $book->item_code = $request->code;
            $book->item_name = $request->name;
            $book->quantity =  $book->quantity += $request->qty;
            $book->stock =  $book->stock += $request->qty;
            $book->price = $request->price;
            $book->save();

            $report = new BookstallStockReport();
            $report->item_id = $book->id;
            $report->quantity = $request->qty;
            $report->price = $book->price;
            $report->stock = $request->qty;
            $report->save();
            DB::commit();
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
        }
    }
}
