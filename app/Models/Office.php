<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class Office extends Model
{
    protected $guarded = [];
    protected  $table = 'offices';

    public static function save_info($request)
    {
        $rand = rand(5, 10);
        if($request->hasFile('photo')){
        $customerName = $request->name;
        $customer = new Office;
        $customer->name = $customerName;
        $customer->initiated_name = $request->initiated_name;
        $customer->goatra = $request->goatra;
        $customer->address = $request->address;
        $customer->pranami = $request->pranami;
        $customer->festival_name = $request->festival_name;
        $customer->qualification = $request->qualification;
        $customer->occupation = $request->occupation;
        $customer->collector = $request->collector;
        $customer->department = $request->department;
        $customer->date_of_birth = $request->date_of_birth;
        $customer->phone = $request->phone;
        $customer->age = $request->age;
        $customer->date = $request->date;
        if ($request->hasFile('photo')) {
            if ($customer->photo) {
                unlink($customer->photo);
            }
            $image = $request->file('photo');
            $imageName = $image->getClientOriginalName();
            $fileName = trim($customerName) . "_" . $rand . $imageName;
            $directory = 'public/photo/';
            $imageUrl = $directory . $fileName;
            Image::make($image)->resize(320, 240)->save($imageUrl);
            $customer->photo = $imageUrl;
        }
        $customer->save();
        } else{
            $customerName = $request->name;
            $customer = new Office;
            $customer->name = $customerName;
            $customer->goatra = $request->goatra;
            $customer->address = $request->address;
            $customer->pranami = $request->pranami;
            $customer->festival_name = $request->festival_name;
            $customer->qualification = $request->qualification;
            $customer->occupation = $request->occupation;
            $customer->collector = $request->collector;
            $customer->department = $request->department;
            $customer->date_of_birth = $request->date_of_birth;
            $customer->phone = $request->phone;
            $customer->age = $request->age;
            $customer->initiated_name = $request->initiated_name;
            $customer->date = $request->date;
            $customer->save();
        }
    }

    public static function update_info($request)
    {
        if ($request->hasFile('photo')) {
            $rand = rand(5, 10);
            $customerName = $request->name;
            $customer = Office::find($request->id);
            $customer->name = $customerName;
            $customer->initiated_name = $request->initiated_name;
            $customer->date = $request->date;
            $customer->goatra = $request->goatra;
            $customer->address = $request->address;
            $customer->pranami = $request->pranami;
            $customer->festival_name = $request->festival_name;
            $customer->qualification = $request->qualification;
            $customer->occupation = $request->occupation;
            $customer->collector = $request->collector;
            $customer->department = $request->department;
            $customer->date_of_birth = $request->date_of_birth;
            $customer->phone = $request->phone;
            $customer->age = $request->age;
            if ($request->hasFile('photo')) {
                if ($customer->photo) {
                    unlink($customer->photo);
                }
                $image = $request->file('photo');
                $imageName = $image->getClientOriginalName();
                $fileName = trim($customerName) . "_" . $rand . $imageName;
                $directory = 'public/photo/';
                $imageUrl = $directory . $fileName;
                Image::make($image)->resize(320, 240)->save($imageUrl);
                $customer->photo = $imageUrl;
            }
        } else {
            $customerName = $request->name;
            $customer = Office::find($request->id);
            $customer->name = $customerName;
            $customer->initiated_name = $request->initiated_name;
            $customer->date = $request->date;
            $customer->goatra = $request->goatra;
            $customer->address = $request->address;
            $customer->pranami = $request->pranami;
            $customer->festival_name = $request->festival_name;
            $customer->qualification = $request->qualification;
            $customer->occupation = $request->occupation;
            $customer->collector = $request->collector;
            $customer->department = $request->department;
            $customer->date_of_birth = $request->date_of_birth;
            $customer->phone = $request->phone;
            $customer->age = $request->age;
        }
        $customer->update();
    }
}
