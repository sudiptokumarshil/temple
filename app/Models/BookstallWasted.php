<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BookstallItem;
use DB;
use Auth;
class BookstallWasted extends Model
{
    protected $guarded = [];

    public static function wastedCreateInfo($request)
    {
      DB::beginTransaction();
      try {
        $saveWitem = new BookstallWasted();
        $saveWitem->item_id = $request->item_id;
        $saveWitem->quantity = $request->quantity;
        $saveWitem->remarks = $request->remarks;
        $saveWitem->created_by = Auth::user()->id;
        $saveWitem->save();

        BookstallItem::where('id',$request->item_id)->decrement('stock',abs($request->quantity));
        DB::commit();
          // all good
      } catch (\Exception $e) {
          DB::rollback();
          // something went wrong
      }
    }
}
