<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BakeryItem;
use DB;
use Auth;

class BakeryWasteditem extends Model
{
    protected $guarded = [];
    protected $table = 'bakery_wasteds';

    public static function wastedCreateInfo($request)
    {
        DB::beginTransaction();
        try {
            $saveWitem = new BakeryWasteditem();
            $saveWitem->item_id = $request->item_id;
            $saveWitem->quantity = $request->quantity;
            $saveWitem->remarks = $request->remarks;
            $saveWitem->created_by = Auth::user()->id;
            $saveWitem->save();

            BakeryItem::where('id', $request->item_id)->decrement('stock', abs($request->quantity));
            DB::commit();
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
        }
    }
}
