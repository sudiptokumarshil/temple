<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\BookstallItem;
use App\Models\Book_invoice;
use App\Models\BookstallStockReport;
use Illuminate\Http\Request;
use Session;
use Cart;
use DB;
use Auth;

class BookItemsell extends Model
{
    protected $guarded = [];

    public static function bookstall_confim_order($request)
    {
        DB::beginTransaction();
        try {

            $bookitem = new BookItemsell();
            $bookitem->customer_name = $request->customer_name;
            $bookitem->mobile_number = $request->mobile_number;
            $bookitem->paid = $request->paid;
            $bookitem->due = $request->due;
            $bookitem->date = $request->date;
            $bookitem->quantity = Cart::count();
            // $bookitem->price = Cart::priceTotal();
            $bookitem->price = $request->price;
            $bookitem->address = $request->address;
            $bookitem->discount = intval($request->discount);
            $bookitem->created_by = Auth::user()->id;
            $bookitem->save();

            $cartProduct = Cart::content();
            foreach ($cartProduct as $v_cart) {
                $pinfo = BookstallItem::find($v_cart->id);
                $invoice = new Book_invoice();
                $invoice->bookSales_id = $bookitem->id;
                $invoice->item_id = $v_cart->id;
                $invoice->unit_price = $v_cart->price;
                $invoice->qty = $v_cart->qty;
                $invoice->save();
                BookstallItem::where('id', $v_cart->id)->decrement('stock', abs($invoice->qty));
                BookstallItem::where('id', $v_cart->id)->increment('sold', abs($invoice->qty));
            }

            Cart::destroy();
            DB::commit();
            // all good
        } catch (\Exception $e) {
            DB::rollback();
            // something went wrong
        }
    }
}
