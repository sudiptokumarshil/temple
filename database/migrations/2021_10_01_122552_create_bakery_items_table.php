<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBakeryItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bakery_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('item_code', 100)->nullable();
            $table->string('item_name', 100)->nullable();
            $table->integer('quantity')->default(0);
            $table->double('price', 10, 2)->nullable();
            $table->integer('stock')->default(0);
            $table->integer('sold')->default(0);
            $table->date('date')->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bakery_items');
    }
}
