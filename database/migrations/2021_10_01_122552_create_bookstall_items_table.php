<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookstallItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookstall_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('item_code', 20)->nullable();
            $table->string('item_name',100)->nullable();
            $table->integer('quantity')->default(0);
            $table->integer('stock')->default(0);
            $table->integer('sold')->default(0);
            $table->double('price',10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookstall_items');
    }
}
