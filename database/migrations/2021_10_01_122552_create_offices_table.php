<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('goatra', 100)->nullable();
            $table->string('phone', 80)->nullable();
            $table->string('name', 100)->nullable();
            $table->string('festival_name', 100)->nullable();
            $table->date('date_of_birth');
            $table->double('pranami', 10, 2)->nullable();
            $table->string('department', 100)->nullable();
            $table->string('collector', 100)->nullable();
            $table->text('address')->nullable();
            $table->text('occupation')->nullable();
            $table->text('qualification')->nullable();
            $table->string('photo', 100)->nullable();
            $table->integer('age')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offices');
    }
}
