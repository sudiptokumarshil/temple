<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBakeryItemsSellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bakery_items_sells', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('item_name', 100)->nullable();
            $table->unsignedBigInteger('item_id')->nullable();
            $table->integer('quantity')->default(0);
            $table->double('price', 10, 2)->nullable();
            $table->string('customer_name', 100)->nullable();
            $table->string('phone', 80)->nullable();
            $table->double('paid', 10, 2)->nullable();
            $table->double('due', 10, 2)->nullable();
            $table->date('date')->nullable();
            $table->double('discount', 10, 2)->nullable();
            $table->text('address')->nullable();
            $table->string('mobile_number', 80)->nullable();
            $table->unsignedBigInteger('created_by')->default(0);
            $table->unsignedBigInteger('updated_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bakery_items_sells');
    }
}
