<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookItemsellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_itemsells', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('item_name')->nullable();
            $table->integer('quantity')->default(0);
            $table->double('price', 10, 2)->nullable();
            $table->string('customer_name',100)->nullable();
            $table->text('address')->nullable();
            $table->string('mobile_number', 80)->nullable();
            $table->double('paid',10, 2)->nullable();
            $table->double('due',10, 2)->nullable();
            $table->double('discount', 10, 2)->nullable();
            $table->date('date')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_itemsells');
    }
}
