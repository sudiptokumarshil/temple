<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100)->nullable();
            $table->string('gotra', 100)->nullable();
            $table->text('address')->nullable();
            $table->text('qualification')->nullable();
            $table->unsignedBigInteger('member_id')->nullable();
            $table->double('pranami',10, 2)->nullable();
            $table->text('occupation')->nullable();
            $table->string('counsellor', 100)->nullable();
            $table->string('blood_group', 100)->nullable();
            $table->integer('age')->default(0);
            $table->string('department', 100)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('phone', 80)->nullable();
            $table->string('customer_image', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
