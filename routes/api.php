<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([

    'middleware' => 'api',
    // 'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth'

], function () {

    Route::post('login', 'Api\AuthController@login');
    Route::post('logout', 'Api\AuthController@logout');
    Route::post('refresh', 'Api\AuthController@refresh');
    Route::post('me', 'Api\AuthController@me');
    Route::post('payload', 'Api\AuthController@payload');
    Route::post('register', 'Api\AuthController@register');


});
//Route::group(['middleware' => ['auth:api']], function () {

//    for bookstall part......
Route::post('add-new-book', 'Api\BookstallController@addnewitem');
Route::get('get-book-item', 'Api\BookstallController@getbook_item');
Route::get('edit-book-item/{id}', 'Api\BookstallController@editbook_item');
Route::post('update-book-item', 'Api\BookstallController@updatebook_item');
Route::post('book-item-sales-form', 'Api\BookstallController@book_item_sales_form');
Route::get('get-all-sales-book', 'Api\BookstallController@getbook_item_sells');
Route::get('details-sales-item/{id}', 'Api\BookstallController@book_sales_item_details');


//for bakery  part.......................
Route::post('add-new-item', 'Api\BakeryController@add_item');
Route::get('all-item', 'Api\BakeryController@index');
Route::get('edit-item/{id}', 'Api\BakeryController@edit_item');
Route::patch('update-item/{id}', 'Api\BakeryController@update_item');
//bakery item sells form ....
Route::post('item-sales-form', 'Api\BakeryController@add_sales_item');
Route::get('get-all-sales-item', 'Api\BakeryController@show_all_sells_item');
Route::get('details-sales-bakery-item/{id}', 'Api\BookstallController@details_bakery_item');

// for office
Route::post('add-contact-form', 'Api\OfficeController@add_contact_view_form');




//});

