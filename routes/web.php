<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function () {

    echo 333;

    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    return "Cache Clear Successfully!!";
});

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes(['register' => false]);
Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::group(['middleware' => ['bakery']], function () {

        // ************************************* start bakery item *************************************
        Route::get('/add-item', 'Bakery\BakeryController@index')->name('add-item');
        Route::post('/save-item', 'Bakery\BakeryController@save_item')->name('save-item');
        Route::get('/all-item', 'Bakery\BakeryController@all_item')->name('all-item');
        Route::get('/edit-item/{id}', 'Bakery\BakeryController@edit_item')->name('edit-item');
        Route::get('/bakery-inedit/{id}', 'Bakery\BakeryController@edit_item_inv')->name('bakery-inedit');
        Route::patch('/update-item/{id}', 'Bakery\BakeryController@update_item')->name('update-item');
        Route::get('/bakery-report', 'Bakery\BakeryController@report')->name('bakery-report');
        Route::post('/breport-result', 'Bakery\BakeryController@report_result')->name('breport-result');
        Route::post('/bakery-customer-suggest', 'Bakery\BakeryController@customer_suggest')->name('bakery-customer-suggest');
        Route::get('/bakery-invoice/{id}', 'Bakery\SaleController@bakery_invoice')->name('bakery-invoice');
        Route::post('/bakery-result', 'Bakery\BakeryController@bakery_result')->name('bakery-result');
        Route::get('/bakery-stock-report', 'Bakery\BakeryController@bakeyr_stock_report')->name('bakery-stock-report');
        Route::post('/bakery-itemstock-result', 'Bakery\BakeryController@bakeyr_stock_report_result')->name('bakery-itemstock-result');
        Route::patch('/bakery-inupdate/{id}', 'Bakery\SaleController@bookstall_invoice_update')->name('bakery-inupdate');
        // for Bakery Wasted Item ............................................
        Route::get('/bakery-wasted', 'Bakery\WastedItemController@index')->name('bakery-wasted');
        Route::post('/save-bakery-wasted', 'Bakery\WastedItemController@create')->name('save-bakery-wasted');
        Route::get('/bakerytock-details/{id}', 'Bakery\BakeryController@stock_report_details')->name('bakerytock-details');
    });


    Route::group(['middleware' => ['bakerySalesman']], function () {
        // for sale part ...........
        Route::get('/bakery-sales', 'Bakery\SaleController@index')->name('bakery-sales');
        // shopping bag for bakery ................
        Route::get('/bakery-add-cart', 'Bakery\CartController@bakery_add_to_cart')->name('bakery-add-cart');
        Route::get('/bakery-salesform', 'Bakery\BakeryController@bakery_sales_form')->name('bakery-salesform');
        Route::get('/bakery-delete-cart/{id}', 'Bakery\CartController@delete_cart')->name('bakery-delete-cart');
        Route::get('/bakery-update-cart', 'Bakery\CartController@update_cart')->name('bakery-update-cart');
        Route::get('/bakery-invoice-print/{id}', 'Bakery\SaleController@bakery_invoice_print')->name('bakery-invoice-print');
        Route::get('/all-item', 'Bakery\BakeryController@all_item')->name('all-item');
        Route::post('/bakery-confim-order', 'Bakery\SaleController@bakery_confim_order')->name('bakery-confim-order');


    });

    // ************************************* end bakery item ***************************************
    Route::group(['middleware' => ['bookstall']], function () {
        // ************************************start bookstall part ************************************
        Route::get('/add-book', 'Bookstall\BookstallController@index')->name('add-book');
        Route::post('/save-book', 'Bookstall\BookstallController@save_book')->name('save-book');
        Route::get('/edit-book/{id}', 'Bookstall\BookstallController@edit_book')->name('edit-book');
        Route::patch('/update-book/{id}', 'Bookstall\BookstallController@update_book')->name('update-book');
        Route::get('/bookstall-report', 'Bookstall\BookstallController@report')->name('bookstall-report');
        Route::post('/bookreport-result', 'Bookstall\BookstallController@report_result')->name('bookreport-result');

        Route::get('/itemstock-report', 'Bookstall\BookstallController@itemstock_report')->name('itemstock-report');
        Route::post('/itemstock-result', 'Bookstall\BookstallController@itemstock_report_result')->name('itemstock-result');
        Route::get('/itemstock-details/{id}', 'Bookstall\BookstallController@stock_report_details')->name('itemstock-details');
        Route::post('/itemsuggest', 'Bookstall\BookstallController@item_match_result')->name('itemsuggest');
        //    bookstall invoice edit
        Route::get('/bookstall-inedit/{id}', 'Bookstall\BookstallController@bookstall_invoice_edit')->name('bookstall-inedit');
        Route::patch('/bookstall-inupdate/{id}', 'Bookstall\BookstallController@bookstall_invoice_update')->name('bookstall-inupdate');

        // for Bookstall Wasted Item ............................................
        Route::get('/bookstall-wasted', 'Bookstall\WastedItemController@index')->name('bookstall-wasted');
        Route::post('/save-bookstall-wasted', 'Bookstall\WastedItemController@create')->name('save-bookstall-wasted');
    });
    Route::group(['middleware' => ['bookstallSalesman']], function () {
        Route::get('/bookstall-sale', 'Bookstall\BookstallController@bookstall_sale')->name('bookstall-sale');
        Route::get('/all-book', 'Bookstall\BookstallController@all_book')->name('all-book');
        Route::get('/bookstall-invoice/{id}', 'Bookstall\BookstallController@bookstall_invoice')->name('bookstall-invoice');
        Route::get('/bookstall-invoice-print/{id}', 'Bookstall\BookstallController@bookstall_invoice_print')->name('bookstall-invoice-print');
        Route::get('/bookstall-salesform', 'Bookstall\BookstallController@book_sales_form')->name('bookstall-salesform');
        Route::post('/bookstall-confim-order', 'Bookstall\BookstallController@bookstall_confim_order')->name('bookstall-confim-order');
        Route::post('/book-result', 'Bookstall\BookstallController@book_result')->name('book-result');
        Route::post('/book-filter', 'Bookstall\BookstallController@book_filter')->name('book-filter');
        Route::post('/customer-suggest', 'Bookstall\BookstallController@customer_suggest')->name('customer-suggest');
        // Route::get('/bookstall-sale', 'Bookstall\BookstallController@bookstall_sale')->name('bookstall-sale');

        // Shopping Bag Part for Bookstall ............................................................
        Route::get('/add-to-cart', 'Bookstall\CartController@add_to_cart')->name('add-to-cart');
        Route::get('/isCartExist', 'Bookstall\CartController@isCartExist');
        Route::get('/delete-cart/{id}', 'Bookstall\CartController@delete_cart')->name('delete-cart');
        Route::get('/update-cart', 'Bookstall\CartController@update_cart')->name('update-cart');
        // end Shopping Cart Part for Bookstall.......................................................
    });
    // ******************************************end bookstall part ***********************************
    Route::group(['middleware' => ['nityaseva']], function () {
        // ********************************start nitya seva part ******************************************
        Route::get('/add-customer', 'NityaSeva\NityaSevaController@index')->name('add-customer');
        Route::get('/edit-customer/{id}', 'NityaSeva\NityaSevaController@edit_customer')->name('edit-customer');
        Route::post('/update-customer', 'NityaSeva\NityaSevaController@update_customer')->name('update-customer');
        Route::post('/save-customer', 'NityaSeva\NityaSevaController@save_customer')->name('save-customer');
        Route::get('/all-customer', 'NityaSeva\NityaSevaController@all_customer')->name('all-customer');
        Route::get('/add-pranami', 'NityaSeva\NityaSevaController@add_pranami')->name('add-pranami');
        Route::post('/save-pranami', 'NityaSeva\NityaSevaController@save_pranami')->name('save-pranami');
        Route::get('/pranami-report', 'NityaSeva\NityaSevaController@report')->name('pranami-report');
        Route::get('/edit-pranami/{id}', 'NityaSeva\NityaSevaController@edit_pranami');
        Route::post('/preport-result', 'NityaSeva\NityaSevaController@report_result')->name('preport-result');

        //************************************  end nitya seva part *************************************
    });
    Route::group(['middleware' => ['office']], function () {
        // ***************************** start office part **************************************************
        Route::get('/manage-contact', 'Office\OfficeController@index')->name('manage-contact');
        Route::get('/add-contact', 'Office\OfficeController@add_contact')->name('add-contact');
        Route::post('/save-contact', 'Office\OfficeController@save_contact')->name('save-contact');
        Route::get('/edit-contact/{id}', 'Office\OfficeController@edit')->name('edit-contact');
        Route::post('/update-contact', 'Office\OfficeController@update_contact')->name('update-contact');
        Route::get('/manage-office-member', 'Office\OfficeMemberController@index')->name('manage-office-member');
        Route::get('/create-office-member', 'Office\OfficeMemberController@create')->name('create-office-member');
        Route::get('/edit-office-member/{id}', 'Office\OfficeMemberController@edit')->name('edit-office-member');
        Route::post('/store-office-member', 'Office\OfficeMemberController@store')->name('store-office-member');
        Route::post('/update-office-member', 'Office\OfficeMemberController@update')->name('update-office-member');
        Route::post('/filter-office-member', 'Office\OfficeMemberController@filter')->name('filter-office-member');

        // ***************************** start office part **************************************************
    });
    Route::group(['middleware' => ['admin']], function () {
        // ==================================== start for user role and permission ===========================================
        Route::get('/manage-role', 'Admin\RoleController@index')->name('manage-role');
        Route::get('/add-role', 'Admin\RoleController@add_role')->name('add-role');
        Route::post('/save-role', 'Admin\RoleController@save_role')->name('save-role');
        Route::get('/edit-role/{id}', 'Admin\RoleController@edit')->name('edit-role');
        Route::post('/update-role', 'Admin\RoleController@update_role')->name('update-role');
        Route::get('/change-password/{id}', 'Admin\RoleController@change_password')->name('change-password');
        Route::post('/update-password', 'Admin\RoleController@update_password')->name('update-password');
        // ==================================== end for user role and permission ===========================================
    });
});
