-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2021 at 04:53 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `iskconna_jogonnath_erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `bakeryinvoices`
--

CREATE TABLE `bakeryinvoices` (
  `id` int(11) NOT NULL,
  `qty` varchar(255) NOT NULL,
  `bakerysales_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `unit_price` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bakery_items`
--

CREATE TABLE `bakery_items` (
  `id` bigint(20) NOT NULL,
  `item_code` varchar(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `quantity` int(15) NOT NULL,
  `price` float NOT NULL,
  `stock` int(11) NOT NULL DEFAULT 0,
  `sold` int(11) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp(),
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bakery_items_sells`
--

CREATE TABLE `bakery_items_sells` (
  `id` bigint(20) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_id` int(11) DEFAULT 0,
  `quantity` int(15) NOT NULL,
  `price` float NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `paid` float DEFAULT 0,
  `due` float DEFAULT 0,
  `date` date DEFAULT NULL,
  `discount` float NOT NULL,
  `address` text NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT 0,
  `updated_by` int(11) NOT NULL DEFAULT 0,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bakery_stock_report`
--

CREATE TABLE `bakery_stock_report` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `sold` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bakery_wasteds`
--

CREATE TABLE `bakery_wasteds` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `remarks` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `bookstall_items`
--

CREATE TABLE `bookstall_items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `stock` int(11) NOT NULL DEFAULT 0,
  `sold` int(11) NOT NULL DEFAULT 0,
  `price` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bookstall_stock_report`
--

CREATE TABLE `bookstall_stock_report` (
  `id` bigint(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `sold` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bookstall_wasteds`
--

CREATE TABLE `bookstall_wasteds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `remarks` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `book_invoices`
--

CREATE TABLE `book_invoices` (
  `id` int(11) NOT NULL,
  `qty` varchar(255) NOT NULL,
  `bookSales_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `unit_price` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `book_itemsells`
--

CREATE TABLE `book_itemsells` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `due` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `discount` float DEFAULT 0,
  `date` date NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT 0,
  `updated_by` int(10) UNSIGNED DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gotra` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `qualification` text NOT NULL,
  `occupation` text NOT NULL,
  `counsellor` varchar(255) NOT NULL,
  `blood_group` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `department` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `phone` varchar(255) NOT NULL,
  `customer_image` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_08_01_160944_create_admins_table', 1),
(10, '2020_08_02_110531_create_bookstalls_table', 1),
(11, '2020_08_05_045501_create_bookstall_items_table', 2),
(13, '2020_08_05_090706_create_book_itemsells_table', 3),
(14, '2020_08_10_084351_create_bakeries_table', 4),
(15, '2021_01_14_150629_create_bookstall_wasteds_table', 5),
(16, '2021_01_22_113727_create_bakery_items_table', 0),
(17, '2021_01_22_113727_create_bakery_items_sells_table', 0),
(18, '2021_01_22_113727_create_book_invoices_table', 0),
(19, '2021_01_22_113727_create_book_itemsells_table', 0),
(20, '2021_01_22_113727_create_bookstall_items_table', 0),
(21, '2021_01_22_113727_create_bookstall_stock_report_table', 0),
(22, '2021_01_22_113727_create_bookstall_wasteds_table', 0),
(23, '2021_01_22_113727_create_customers_table', 0),
(24, '2021_01_22_113727_create_failed_jobs_table', 0),
(25, '2021_01_22_113727_create_offices_table', 0),
(26, '2021_01_22_113727_create_password_resets_table', 0),
(27, '2021_01_22_113727_create_pranamies_table', 0),
(28, '2021_01_22_113727_create_users_table', 0),
(29, '2021_01_22_125224_create_bakery_items_table', 0),
(30, '2021_01_22_125224_create_bakery_items_sells_table', 0),
(31, '2021_01_22_125224_create_book_invoices_table', 0),
(32, '2021_01_22_125224_create_book_itemsells_table', 0),
(33, '2021_01_22_125224_create_bookstall_items_table', 0),
(34, '2021_01_22_125224_create_bookstall_stock_report_table', 0),
(35, '2021_01_22_125224_create_bookstall_wasteds_table', 0),
(36, '2021_01_22_125224_create_customers_table', 0),
(37, '2021_01_22_125224_create_failed_jobs_table', 0),
(38, '2021_01_22_125224_create_offices_table', 0),
(39, '2021_01_22_125224_create_password_resets_table', 0),
(40, '2021_01_22_125224_create_pranamies_table', 0),
(41, '2021_01_22_125224_create_users_table', 0),
(42, '2021_01_23_173826_create_bakery_items_table', 0),
(43, '2021_01_23_173826_create_bakery_items_sells_table', 0),
(44, '2021_01_23_173826_create_bakery_stock_report_table', 0),
(45, '2021_01_23_173826_create_book_invoices_table', 0),
(46, '2021_01_23_173826_create_book_itemsells_table', 0),
(47, '2021_01_23_173826_create_bookstall_items_table', 0),
(48, '2021_01_23_173826_create_bookstall_stock_report_table', 0),
(49, '2021_01_23_173826_create_bookstall_wasteds_table', 0),
(50, '2021_01_23_173826_create_customers_table', 0),
(51, '2021_01_23_173826_create_failed_jobs_table', 0),
(52, '2021_01_23_173826_create_offices_table', 0),
(53, '2021_01_23_173826_create_password_resets_table', 0),
(54, '2021_01_23_173826_create_pranamies_table', 0),
(55, '2021_01_23_173826_create_users_table', 0);

-- --------------------------------------------------------

--
-- Table structure for table `offices`
--

CREATE TABLE `offices` (
  `id` int(11) NOT NULL,
  `goatra` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `festival_name` varchar(255) NOT NULL,
  `date_of_birth` date NOT NULL,
  `pranami` decimal(50,2) NOT NULL,
  `department` varchar(255) NOT NULL,
  `collector` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `occupation` text NOT NULL,
  `qualification` text NOT NULL,
  `photo` varchar(100) DEFAULT NULL,
  `age` int(11) NOT NULL,
  `created_at` date NOT NULL DEFAULT current_timestamp(),
  `updated_at` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pranamies`
--

CREATE TABLE `pranamies` (
  `id` bigint(20) NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `pranami` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` tinyint(3) NOT NULL COMMENT 'boostall =1 ,bakery =2 admin =3 nityaseva =4 office =5 bookstall&bakery = 6 nityasave&office =7\r\nBakery Salesman = 8 \r\nBookstall Salesman = 9',
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT 'active =1 inactive =2',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `role_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', NULL, '$2y$10$thm.cHMPDVQFPcS18eKEbOCF8CmRStEF0rFZa3E3uL9ah/OAEs2ba', NULL, 3, 1, '2021-03-19 09:44:34', '2021-03-19 09:44:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bakeryinvoices`
--
ALTER TABLE `bakeryinvoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bakery_items`
--
ALTER TABLE `bakery_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bakery_items_sells`
--
ALTER TABLE `bakery_items_sells`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bakery_stock_report`
--
ALTER TABLE `bakery_stock_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bakery_wasteds`
--
ALTER TABLE `bakery_wasteds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookstall_items`
--
ALTER TABLE `bookstall_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookstall_stock_report`
--
ALTER TABLE `bookstall_stock_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookstall_wasteds`
--
ALTER TABLE `bookstall_wasteds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_invoices`
--
ALTER TABLE `book_invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_itemsells`
--
ALTER TABLE `book_itemsells`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offices`
--
ALTER TABLE `offices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pranamies`
--
ALTER TABLE `pranamies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bakeryinvoices`
--
ALTER TABLE `bakeryinvoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bakery_items`
--
ALTER TABLE `bakery_items`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bakery_items_sells`
--
ALTER TABLE `bakery_items_sells`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bakery_stock_report`
--
ALTER TABLE `bakery_stock_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bakery_wasteds`
--
ALTER TABLE `bakery_wasteds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bookstall_items`
--
ALTER TABLE `bookstall_items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bookstall_stock_report`
--
ALTER TABLE `bookstall_stock_report`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bookstall_wasteds`
--
ALTER TABLE `bookstall_wasteds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `book_invoices`
--
ALTER TABLE `book_invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `book_itemsells`
--
ALTER TABLE `book_itemsells`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `offices`
--
ALTER TABLE `offices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pranamies`
--
ALTER TABLE `pranamies`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
