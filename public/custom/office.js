function report() {
    let api = getUrl();
    let name = $("#Name").val();
    let counsellor = $("#Counsellor").val();
    let department = $("#Department").val();

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        type: "POST",
        url: api + "filter-office-member",
        data: {
            name: name,
            counsellor: counsellor,
            department: department,
        },
        success: function (data) {
            $("#example").html(data);
        },
    });
}
