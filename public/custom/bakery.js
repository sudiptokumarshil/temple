// add item..
function addItem(v) {
    let api = getUrl();
    let code = $("#item_code").val();
    let name = $("#item_name").val();
    let qty = $("#quantity").val();
    let price = $("#price").val();
    let date = $("#date").val();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    if (code <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Item Code Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (name === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Item Name Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (qty <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Item Quantity Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (price <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Item Price Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (date <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Date Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else {
        $.ajax({
            type: "POST",
            url: api + "save-item",
            data: {
                code: code,
                name: name,
                qty: qty,
                price: price,
                date: date,
            },
            success: function (data) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: "top-end",
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener("mouseenter", Swal.stopTimer);
                        toast.addEventListener("mouseleave", Swal.resumeTimer);
                    },
                });

                Toast.fire({
                    icon: "success",
                    title: "Item Added Successfully!!",
                });

                $("#item_code").val("");
                $("#item_name").val("");
                $("#quantity").val(0);
                $("#price").val(0);
                $("#date").val("");
            },
        });
    }
}
function updateItem(v) {
    let api = getUrl();
    let id = $("#item_id").val();
    let code = $("#item_code").val();
    let name = $("#item_name").val();
    let qty = $("#quantity").val();
    let price = $("#price").val();
    let date = $("#date").val();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    if (code <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Item Code Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (name === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Item Name Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (qty <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Item Quantity Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (price <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Item Price Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (date <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Date Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else {
        $.ajax({
            type: "Patch",
            url: api + "update-item/" + id,
            data: {
                code: code,
                name: name,
                qty: qty,
                price: price,
                date: date,
            },
            success: function (data) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: "top-end",
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener("mouseenter", Swal.stopTimer);
                        toast.addEventListener("mouseleave", Swal.resumeTimer);
                    },
                });

                Toast.fire({
                    icon: "success",
                    title: "Item Updated Successfully!!",
                });
                location.href = api + "all-item";
            },
        });
    }
}
function report(v) {
    let api = getUrl();
    let customer_name = $("#customer_name").val();
    let start_date = $("#start_date").val();
    let end_date = $("#end_date").val();

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        type: "POST",
        url: api + "breport-result",
        data: {
            customer_name: customer_name,
            start_date: start_date,
            end_date: end_date,
        },
        success: function (data) {
            $("#reportResult").html(data);
        },
    });
}

// bakery add to bag ..............
function bakeryaddToCart(v) {
    let id = $(v).attr("data-id");
    let qty = $("#qty-" + id).val();
    let bakery = $("#bakery-" + id).val();

    var btn_txt = $(v).text();
    $(v).text("Adding....");
    $(v).attr("disabled", true);
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    var api = getUrl();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: api + "bakery-add-cart",
        data: { id: id, qty: qty, bakery: bakery },
        success: function (data) {
            $(".cart_qty").text(data.count);

            $(v).text(btn_txt);
            $(v).attr("disabled", false);

            const Toast = Swal.mixin({
                toast: true,
                position: "top-end",
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener("mouseenter", Swal.stopTimer);
                    toast.addEventListener("mouseleave", Swal.resumeTimer);
                },
            });

            Toast.fire({
                icon: "success",
                title: "Cart Added Successfully!!",
            });
        },
    });
}

function increaseValue(v) {
    let id = $(v).attr("data-id");
    let rowId = $("#rowId-" + id).val();
    let item_id = $("#itemIid-" + id).val();
    let value = parseInt(document.getElementById("qty-" + id).value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById("qty-" + id).value = value;

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        url: "./bakery-update-cart",
        type: "GET",
        dataType: "json",
        data: { increment: true, qty: value, rowId: rowId, item_id: item_id },
        success: function (data) {
            if (data.status == 1) {
                Swal.fire({
                    icon: "error",
                    title: "Oops... Stock Out",
                });
                let id = $(v).attr("data-id");
                let iQty = $("#qty-" + id).val();
                $("#qty-" + id).val(iQty - 1);
            } else {
                updateCartInfo(data);
            }
        },
    });
}
function decreaseValue(v) {
    let id = $(v).attr("data-id");
    let value = parseInt(document.getElementById("qty-" + id).value, 10);
    value = isNaN(value) ? 0 : value;
    value < 1 ? (value = 1) : "";
    value--;
    document.getElementById("qty-" + id).value = value;

    let rowId = $("#rowId-" + id).val();
    let qty = $("#qty-" + id).val();
    let item_id = $("#itemIid-" + id).val();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        url: "./bakery-update-cart",
        type: "GET",
        dataType: "json",
        data: { increment: true, qty: qty, rowId: rowId, item_id: item_id },
        success: function (data) {
            if (qty <= 0) $(".tr-" + rowId).remove();
            updateCartInfo(data);
        },
    });
}

function deleteCart(v) {
    let id = $(v).attr("data-id");
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $.ajax({
        type: "get",
        dataType: "json",
        url: "./bakery-delete-cart/" + id,
        data: { id: id },
        success: function (data) {
            $(".tr-" + id).remove();
            updateCartInfo(data);
            const Toast = Swal.mixin({
                toast: true,
                position: "top-end",
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener("mouseenter", Swal.stopTimer);
                    toast.addEventListener("mouseleave", Swal.resumeTimer);
                },
            });

            Toast.fire({
                icon: "success",
                title: "Cart Removed Successfully!!",
            });
        },
    });
}

function orderCheckout(v) {
    let customer_name = $("#customer_name").val();
    let mobile_number = $("#mobile_number").val();
    let paid = $("#paid").val();
    let due = $("#due").val();
    let date = $("#date").val();
    let discount = $("#discount").val();
    let address = $("#address").val();
    let price = $("#netPayable").val();
    let api = getUrl();
    if (customer_name === "") {
        alert("Customer Name Cannot Be Empty");
    } else if (mobile_number === "") {
        alert("Customer Mobile Number Cannot Be Empty");
    } else if (date === "") {
        alert("Date Cannot Be Empty");
    } else if (address === "") {
        alert("Address Cannot Be Empty");
    } else {
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        $(".btn-loader").text("Loading..");
        $.ajax({
            type: "POST",
            url: api + "bakery-confim-order",
            data: {
                customer_name: customer_name,
                mobile_number: mobile_number,
                paid: paid,
                due: due,
                price: price,
                date: date,
                discount: discount,
                address: address,
            },
            success: function (data) {
                $(".btn-loader").text("");
                bakerySuccessMessage(data);
            },
        });
    }
    // });
}
const bakerySuccessMessage = (data) => {
    let api = getUrl();
    let timerInterval;
    Swal.fire({
        title: "Your order created successfully!!",
        html: "We will close in <b></b> milliseconds.",
        timer: 2000,
        timerProgressBar: true,
        onBeforeOpen: () => {
            Swal.showLoading();
            timerInterval = setInterval(() => {
                const content = Swal.getContent();
                if (content) {
                    const b = content.querySelector("b");
                    if (b) {
                        b.textContent = Swal.getTimerLeft();
                    }
                }
            }, 100);
        },
    }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
            if (data.type == 8) {
                window.location.href = api + "home";
            } else {
                window.location.href = api + "bakery-report";
            }
        }
    });
};

function updateCartInfo(data) {
    $("#here").load("#here> *");
    $(".cart_qty").text(data.count);
    var gross_discount = 0;
    var gross_amount = 0;
    $.each(data.list, function (key, val) {
        var discount = 0;
        var amount = val.price * val.qty;
        if (val.options.discount != null && val.options.discount > 0)
            discount = val.options.discount * val.qty;
        else {
            var dis_amt = (amount * val.options.percent) / 100;
            discount = dis_amt;
        }
        var sub_amount = parseFloat(amount) - parseFloat(discount);
        gross_amount = parseFloat(gross_amount) + parseFloat(amount);
        $(".amount-" + val.rowId).text(amount + " BDT");
        $(".discount-" + val.rowId).text(discount + " BDT");
        $(".sub_amount-" + val.rowId).text(sub_amount + " BDT");
        gross_discount = parseFloat(gross_discount) + parseFloat(discount);
    });
    var total = parseFloat(gross_amount) - parseFloat(gross_discount);
    $(".final_amount_before").val(total);
}

$(document).on("keyup", ".customer_info", function () {
    var api = getUrl();
    var vv = $(this);
    var key = $(this).val();
    const method = "POST";
    const url = api + "bakery-customer-suggest";
    const data = {
        query: key,
    };
    ajaxSetup(
        function (data) {
            $(vv).autocomplete({
                source: data,
                select: function (e, ui) {
                    // alert(ui.item.id);
                    var bookId = ui.item.id;
                },
            });
        },
        method,
        url,
        data
    );
});

function editInvoice(id) {
    let api = getUrl();
    $.ajax({
        url: api + "bakery-inedit/" + id,
        type: "GET",
        data: {
            _token: "{{ csrf_token() }}",
        },
        success: function (data) {
            $("#paid").val(data.paid);
            $("#checkPaid").val(data.paid);
            $("#due").val(data.due);
            $("#total").val(data.price);
            $("#invId").val(data.id);
        },
    });
}

function itemStockreport(v) {
    let api = getUrl();
    let item_info = $("#item_info").val();
    let from = $("#from").val();
    let to = $("#to").val();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        type: "POST",
        url: api + "bakery-itemstock-result",
        data: { item_info: item_info, from: from, to: to },
        success: function (data) {
            $("#bakery_itemstock_result").html(data);
        },
    });
}

function updateInvoice(v) {
    let id = $("#invId").val();
    let paid = $("#paid").val();
    let due = $("#due").val();
    let api = getUrl();
    let check = $('#checkPaid').val();
    if(Number(paid) >= Number(check)){
        $.ajaxSetup({
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
        });
        $.ajax({
            type: "patch",
            url: api + "bakery-inupdate/" + id,
            data: { due: due, paid: paid },
            success: function (data) {
                alert("Updated Successfully!!");
                window.location.reload();
            },
        });
    }else{
        Swal.fire({
            icon: 'error',
            title: 'Present Amount Cannot Be Less Than Previous Amount ',
            text: 'Something went wrong!',
        });
    }

}

function searchItem(v) {
    let api = getUrl();
    let item_info = $("#item_info").val();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        type: "POST",
        url: api + "bakery-result",
        data: { item_info: item_info },
        success: function (data) {
            $("#itemData").html(data);
        },
    });
}

// for bakery Wasted item
function wastedItem(v) {
    let api = getUrl();
    let item_id = $("#item_id").val();
    let quantity = $("#quantity").val();
    let remarks = $("#remarks").val();

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $(".btn-loader").text("Loading..");
    $.ajax({
        type: "POST",
        url: api + "save-bakery-wasted",
        data: { item_id: item_id, quantity: quantity, remarks: remarks },
        success: function (data) {
            $(".btn-loader").text("Save Changes");

            const Toast = Swal.mixin({
                toast: true,
                position: "top-end",
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener("mouseenter", Swal.stopTimer);
                    toast.addEventListener("mouseleave", Swal.resumeTimer);
                },
            });

            Toast.fire({
                icon: "success",
                title: "Item Wasted Added Successfully!!",
            });
            location.reload();
            $("#item_id").val(0);
            $("#quantity").val(0);
            $("#remarks").val("");
        },
        error: function (xhr) {
            $("#validation-errors").html("");
            $.each(xhr.responseJSON.errors, function (key, value) {
                $("#validation-errors").append(
                    '<div class="alert alert-danger">' + value + "</div"
                );
            });
        },
    });
}
