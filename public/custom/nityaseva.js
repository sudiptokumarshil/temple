// add item..
function addCustomer(v) {
    let api = getUrl();
    let name = $("#name").val();
    let id = $("#cutomer_id").val();
    let gotra = $("#gotra").val();
    let address = $("#address").val();
    let qualification = $("#qualification").val();
    let counsellor = $("#counsellor").val();
    let blood_group = $("#blood_group").val();
    let age = $("#age").val();
    let department = $("#department").val();
    let date_of_birth = $("#date_of_birth").val();
    let phone = $("#phone").val();
    let occupation = $("#occupation").val();
    let photo = $("#file-upload").val();

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    if (name === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Name Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (gotra === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Gotra Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (address === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Address  Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (qualification === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Qualification Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (occupation === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Occupation Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (counsellor === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Counsellor Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (blood_group === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Blood Group Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (age <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Age Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (department === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Department Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (date_of_birth === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "BirthDate Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (phone === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Phone Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else {
        $.ajax({
            type: "POST",
            url: api + "save-customer",
            data: {
                name: name,
                gotra: gotra,
                address: address,
                qualification: qualification,
                counsellor: counsellor,
                blood_group: blood_group,
                age: age,
                department: department,
                date_of_birth: date_of_birth,
                phone: phone,
                occupation: occupation,
                id: id,
                photo:photo
            },
            success: function (data) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: "top-end",
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener("mouseenter", Swal.stopTimer);
                        toast.addEventListener("mouseleave", Swal.resumeTimer);
                    },
                });

                Toast.fire({
                    icon: "success",
                    title: data.message,
                });
                location.href = api + "all-customer";
            },
        });
    }
}
function addPranami(v) {
    let api = getUrl();
    let customer_id = $("#customer_id").val();
    let id = $("#pranami_ids").val();
    let pranami = $("#pranami").val();
    let date = $("#date").val();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    if (customer_id <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Customer Name Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (pranami <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Pranami Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (date <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Date Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else {
        $.ajax({
            type: "POST",
            url: api + "save-pranami",
            data: {
                customer_id: customer_id,
                pranami: pranami,
                date: date,
                id: id,
            },
            success: function (data) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: "top-end",
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener("mouseenter", Swal.stopTimer);
                        toast.addEventListener("mouseleave", Swal.resumeTimer);
                    },
                });

                Toast.fire({
                    icon: "success",
                    title: data.message,
                });

                location.href = api+ "pranami-report";
            },
        });
    }
}
function updateItem(v) {
    let api = getUrl();
    let id = $("#item_id").val();
    let code = $("#item_code").val();
    let name = $("#item_name").val();
    let qty = $("#quantity").val();
    let price = $("#price").val();
    let date = $("#date").val();
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    if (code <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Item Code Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (name === "") {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Item Name Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (qty <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Item Quantity Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (price <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Item Price Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else if (date <= 0) {
        Swal.fire({
            icon: "error",
            title: "Oops...",
            text: "Date Cannot Be Empty!",
            footer: "<a href>Why do I have this issue?</a>",
        });
    } else {
        $.ajax({
            type: "Patch",
            url: api + "update-item/" + id,
            data: {
                code: code,
                name: name,
                qty: qty,
                price: price,
                date: date,
            },
            success: function (data) {
                const Toast = Swal.mixin({
                    toast: true,
                    position: "top-end",
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener("mouseenter", Swal.stopTimer);
                        toast.addEventListener("mouseleave", Swal.resumeTimer);
                    },
                });

                Toast.fire({
                    icon: "success",
                    title: "Item Updated Successfully!!",
                });

                location.href = api+ "all-customer";
            },
        });
    }
}
function pranami_report(v) {
    let api = getUrl();
    let customer_id = $("#customer_ids").val();
    let start_date = $("#start_date").val();
    let end_date = $("#end_date").val();

    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        type: "POST",
        url: api + "preport-result",
        data: {
            customer_id: customer_id,
            start_date: start_date,
            end_date: end_date,
        },
        success: function (data) {
            $("#reportResult").html(data);
        },
    });
}

function editCustomer(id) {
    let api = getUrl();
    $.ajax({
        url: api + "edit-customer/" + id,
        type: "GET",
        data: {
            _token: "{{ csrf_token() }}",
        },
        success: function (data) {
            $("#name").val(data.name);
            $("#cutomer_id").val(data.id).trigger('change');
            $("#gotra").val(data.gotra);
            $("#address").val(data.address);
            $("#qualification").val(data.qualification);
            $("#counsellor").val(data.counsellor);
            $("#blood_group").val(data.blood_group);
            $("#age").val(data.age);
            $("#department").val(data.department);
            $("#date_of_birth").val(data.date_of_birth);
            $("#phone").val(data.phone);
            $("#occupation").val(data.occupation);
        },
    });
}
function editPranami(id) {
    let api = getUrl();
    $.ajax({
        url: api + "edit-pranami/" + id,
        type: "GET",
        data: {
            _token: "{{ csrf_token() }}",
        },
        success: function (data) {
            $("#customer_id").val(data.customer_id).trigger("change");
            $("#pranami_ids").val(data.id);
            $("#pranami").val(data.pranami);
            $("#date").val(data.date);
        },
    });
}
